:<<\_c
. $PR/$PV/basebox/bin/config.fun
_c

:<<\_c
_prepend_file_before_line appends injections such that you can inject more than one and they will execute in sequence
_insert_file_between_lines copies injections over any existing content such that the provisioner can hold default script that may be overwritten by bespoke script
_c


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
function _insert_text_over_lines() {  # <start> <stop> <dfile> <text>

#echo "_insert_text_over_lines @: $@"

1>/dev/null ex $3 <<< "g/$2/
i
${4:-$(</dev/stdin)}
.
wq
"

ex -s -c "/^$1/+0,/^$2/-0 d" -c "wq" "$3"  # delete enclosing lines
}


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
function _insert_text_between_lines() {  # <start> <stop> <dfile> <text>

#echo "_insert_text_between_lines @: $@"

1>/dev/null ex $3 <<< "g/$2/
i
${4:-$(</dev/stdin)}
.
wq
"
}


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
function _insert_file_over_lines() {

ex -s -c "/^$1/+0 r $4" -c "/^$1/+0,/^$2/-0 d" -c "wq" "$3"
}


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
function _insert_file_between_lines() {
#echo ex -s -c "\"/^$1/+1,/^$2/-1 d\" -c \"/^$1/ r $4\" -c \"wq\" $3"
ex -s -c "/^$1/+1,/^$2/-1 d" -c "/^$1/ r $4" -c "wq" "$3"
}
:<<\_x
ex -s -c "/^#PRE_PROVISION_START/+1,/^#PRE_PROVISION_STOP/-1 d" \
  -c "/^#PRE_PROVISION_START/ r ./provisioner.sh.pre" \
  -c "wq" ./provisioner.sh

filename=provisioner.sh
bname=provisioner.sh
_insert_file_between_lines '#PRE_PROVISION_START' '#PRE_PROVISION_STOP' "./$filename" ${bname}.pre
_x


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
:<<\_c
POST_PROVISION="Hello, World!"

_append_text_after_line $start $infile "$POST_PROVISION"
_c

function _append_text_after_line() {  # <line> <dfile> <text>

sed -i "/$1/a \
$3
" "$2"
}


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
:<<\_c
_append_text_over_line "$start" $infile "$POST_PROVISION"
_c

function _append_text_over_line() {  # <line> <dfile> <text>

sed -i "/$1/c \
$3
" "$2"
}


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
:<<\_c
_prepend_text_before_line "$stop" $dfile "<text>"
_c

function _prepend_text_before_line() {  # <line> <file> <text>

#echo "_prepend_text_before_line @: $@"

1>/dev/null ex $2 <<< "g/$1/
i
${3:-$(</dev/stdin)}
.
wq
"
}

:<<\_x
# you cannot pipe to a function
# that means an argument must be used
# however; using an argument prohibits the use of inner quotes

cat > try.txt <<!
#POST_PROVISION_START
#POST_PROVISION_STOP
!

function _prepend_text_before_line() {

declare s=${3:-$(</dev/stdin)}

ex $2 <<< "g/$1/
i
$s
.
wq
"
}

_prepend_text_before_line "#POST_PROVISION_STOP" try.txt "foobar"  # argument

_prepend_text_before_line "#POST_PROVISION_STOP" try.txt <<!  # piped
this "is" some text
!

cat try.txt
_x

:<<\_s
# this fails if the matching string is the last line in the file
sed -i "/$1/i \
$2
" $3
_s


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
function _append_file_after_line() {  # <line> <dfile> <sfile>

#echo "_append_file_after_line @: $@"

1>/dev/null ex $2 <<< "g/$1/+1
i
$(<$3)
.
wq
"
}

:<<\_c
#sed -i -e "/$1/r '$3'" "$2"
sed  -e "/$stop/r $injection" $infile
_c

:<<\_x
cat > try.txt <<!
#POST_PROVISION_START
#POST_PROVISION_STOP
!

cat > injection.txt <<!
foo
"bar"
!

_append_file_after_line "#POST_PROVISION_START" try.txt injection.txt

cat try.txt
_x


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
:<<\_c
if the target file does not end with a newline, then 'ed' appends one and prints to stderr the message 'Newline appended'
to reduce output noise, we use '2>/dev/null' before 'ed'
_c
:<<\_c
https://unix.stackexchange.com/questions/32908/how-to-insert-the-content-of-a-file-into-another-file-before-a-pattern-marker
_c
:<<\_c
$1 - tag
$2 - file
$3 - injection
_c

function _prepend_file_before_line() {  # <line> <dfile> <sfile>

#echo "_prepend_file_before_line @: $@"

1>/dev/null ex $2 <<< "g/$1/
i
$(<$3)
.
wq
"
}

:<<\_x
cat > try.txt <<!
#POST_PROVISION_START
#POST_PROVISION_STOP
!

cat > injection.txt <<!
foo
"bar"
!

_prepend_file_before_line "#POST_PROVISION_STOP" try.txt injection.txt

cat try.txt
_x

:<<\_x
ex -s try.txt <<< "g/#POST_PROVISION_STOP/
i
$(<injection.txt)
.
wq
"

ex -s try.txt <<< "g/#POST_PROVISION_STOP/
i
$(<injection.txt)
.
wq
"

ex -s try.txt <<!
g/#POST_PROVISION_STOP/
i
$(<injection.txt)
.
wq
!

1>/dev/null ex -s try.txt <<!
g/#POST_PROVISION_STOP/
q
!
_x

:<<\_s
these sed solutions fail if the string to match is on the last line of the file
seems best to use something other than sed
#sed -n -i -e "/$1/r $3" -e 1x -e '2,${x;p}' -e '${x;p}' $2
#echo | sed -e "\$d;N;P;/\n${1}/r $3" -e D $2 -
_s
:<<\_c
awk -vf1="$(<$3)" "/$1/{print f1;print;next}1" $2

sed -n -e "/$stop/r $injection" -e 1x -e '2,${x;p}' -e '${x;p}' $infile 
_c

:<<\_x
outfile=test.file

cat > $outfile <<!
#POST_PROVISION_START
#POST_PROVISION_STOP
!

injection=in.file

cat > $injection <<!
Hello, World!
!

#start="^#POST_PROVISION_START"
stop="^#POST_PROVISION_STOP"

. config.fun

_prepend_file_before_line $stop $outfile $injection

_prepend_file_before_line $stop $outfile /dev/stdin <<!
heredoc
!

# pass as stdin
#_prepend_text_before_line $start $stop $outfile <<EOF
_prepend_text_before_line $stop $outfile <<EOF
1
2
EOF

# this resolves to the passing of a file...
_prepend_file_before_line $stop $outfile <(cat <<\injection!
3
4
injection!
)
_x

:<<\_j
echo "hw" | sed 'a\
additional\
lines\
' $outfile

echo "hw" | sed "/$stop/ {
r -
N
}" $outfile


#_prepend_text_before_line $start $stop $outfile <<EOF
_prepend_text_before_line $stop $outfile <<EOF
1
2
EOF

cat <<! | sed -e "/$start/e cat /dev/stdin" $outfile
# added by $(whoami) on $(date)
!

function _prepend_stdin_before_line() {
#local stop="$1"
#local outfile="$2"
#local injection="$3"

sed -e "/$1/e cat /dev/stdin" $2
}

sed -e "/$start/e cat /dev/stdin" $outfile <<!
# added by $(whoami) on $(date)
!

sed -e "/$start/r /dev/stdin" $outfile <<!
# added by $(whoami) on $(date)
!

sed "/$start/r /dev/stdin" $outfile <<!
# added by $(whoami) on $(date)
!

_prepend_file_before_line $stop $outfile /dev/stdin <<!
heredoc
!
_j

:<<\_x
# cf. "Editing files with the ed text editor from scripts.",
# http://wiki.bash-hackers.org/doku.php?id=howto:edit-ed

prepend() {
   printf '%s\n' H 1i "${1}" . wq | ed -s "${2}"
}

echo 'Hello, world!' > myfile
prepend 'line to prepend' myfile
_x

:<<\_x
printf '%s\n' ',s/^/XXX/' w q | ed -s file
_x
