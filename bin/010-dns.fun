:<<\_c
010-dns.fun
_c

#echo "PROOT/$PV/bin/010-dns.fun DNS_ENDPOINT: $DNS_ENDPOINT"

#if [[ "${DNS[host]}" =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then  # explicit ip?

#DNS[host]=${DNS[host]}
#fi


function _dns_get() {

[[ -z "$@" ]] && { echo "<FQDN>"; return 1; }

[[ -z "$DNS_ENDPOINT" ]] && {

local DNS_ENDPOINT=$(_kv_get DNS_ENDPOINT) || { _error "_kv_get DNS_ENDPOINT"; return 1; }
}

_f_url_parse $DNS_ENDPOINT DNS  # -> ${DNS[host]}

local fqdn_dot=$1

#[[ "$fqdn_dot" != *. ]] && {  # does it end with a period (test.picasso.digital.)?
[[ "$fqdn_dot" == *.* ]] || {  # does it include a period (test.picasso.digital.)?

# it does not end with a period

#[[ "$fqdn_dot" == *.* ]] || {

#[[ -n "$FDOM" ]] && fqdn_dot+="-${FDOM/./-}"  # add custom search

[[ -n "$FDOM" ]] && fqdn_dot+="${FDOM:+.$FDOM}."
#}
}

local r="$(nslookup $fqdn_dot ${DNS[host]})"

[[ $? -eq 0 ]] || { _error "DNS entry not found for $1"; return 1; }

r=$(echo "$r" | awk -F':' '/^Address: / {matched = 1} matched {print $2}')
[[ -n "$r" ]] || return 1

echo $r  # return $r
}
export -f _dns_get


# ----------
true
