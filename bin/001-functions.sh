:<<\_c
$PR/$PV/basebox/bin/001-functions.sh

guest and host functions

whenever bash starts executing the conditional in an if statement it disables the errexit feature.
It leaves this disabled until the conditional has been evaluated.
https://delx.net.au/blog/2016/03/safe-bash-scripting-set-e-is-not-enough/
_c

:<<\_x
. $PR/$PV/basebox/bin/001-functions.sh

MNIC_CIDR=192.168.1.6/26
MNIC_IFACE="Intel(R) Wi-Fi 6 AX201 160MHz"

M_SPEC="1)mnic:192.168.1.8/24?bridged#$MNIC_IFACE"
_f_cidr_parse "$M_SPEC"
set | grep mnic=

M_SPEC="1)mnic:'$MNIC_CIDR'?bridged#'$MNIC_IFACE'"
_f_cidr_parse "$M_SPEC"
set | grep mnic=

MQ_SPEC="2)intnet-mq:192.168.57.2/24"
_f_cidr_parse "$MQ_SPEC" MQ
set | grep MQ=
_x


# ----------
:<<\_c
by default aliases are not expanded in non-interactive shells, unless the expand_aliases shell option is set using shopt
_c

shopt -s expand_aliases


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
:<<\_c
_indirect_variable_exists IS_TEST
bash returns this: false or true/undefined
we want bash to return this: false/undefined or true
thus we create this function that returns the results we want
_c

# if we use functions like is_openstack and is_devstack, that requires all the functions to be declared whether they are used or not

# instead we create a generic function that tests our variable's existence and value

function _indirect_variable_exists() { [[ -v $1 ]] && ${!1}; }
export -f _indirect_variable_exists

:<<\_x
. $PR/$PV/basebox/bin/001-functions.sh

export IS_TEST=true
_indirect_variable_exists IS_TEST && echo test

export IS_TEST=false
_indirect_variable_exists IS_TEST && echo test

unset IS_TEST
_indirect_variable_exists IS_TEST && echo test
_x

:<<\_x
dpkg --compare-versions 1.2-3 gt 1.1-4
_x


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
:<<\_c
$1 - ip
$2 - port
_c

function _is_port_active() {
nc -4 -z -w 1 $1 $2
#nmap -p $DB_PORT $IP_HOSTDB | grep -q 'tcp open' || { _error "$IP_HOSTDB:$DB_PORT not open"; return 1; }  # nmap fails on SKYLAKE
#1>/dev/null tcping64.exe -4 -n 1 $IP_HOSTDB $DB_PORT || { _error "tcping64 -4 -n 1 $IP_HOSTDB $DB_PORT"; return 1; }
# return $?
}
export -f _is_port_active

:<<\_x
_is_port_active 192.168.1.20 3306
_x

:<<\_x
uuid=${uuid^^}  # uppercase
uuid=${uuid,,}  # lowercase
od -x /dev/urandom | head -1 | awk '{OFS="-"; srand($6); sub(/./,"4",$5); sub(/./,substr("89ab",rand()*4,1),$6); print $2$3,$4,$5,$6,$7$8$9}'
_x


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
:<<\_c
$1 - ip
_c

function _is_ip() {

local regex="\b(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b"

echo "$1" | egrep "$regex" &>/dev/null

# return $?
}
export -f _is_ip


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
function _reverse_by_separator() {
local in_delimiter=$2
local out_delimiter=${3:-$2}

  tr "${in_delimiter}" $'\n' <<< "${1}" | tac | paste -s -d "$out_delimiter"
}
export -f _reverse_by_separator


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
:<<\_c
see: $PROOF/url/
_c
:<<\_c
When used in a function, `declare' makes NAMEs local, as with the `local' command.  The `-g' option suppresses this behavior.
_c

:<<\_c
this function is intended to conform to the golang URL format, except that it automatically separates domain and port

[scheme:][//[userinfo@]host][/]path[?query][#fragment]

golang:URL.host = www.example.com:443

sdom == subdomain == machine short name - $HOSTNAME or $(hostname -s)
 
# www.example.com:443
ns[host]=www.example.com
ns[port]=443
ns[sdom]=www
ns[domain]=example.com

# 192.168.1.1:443
ns[host]=192.168.1.1
ns[port]=443
ns[sdom]=192-168-1-1
ns[domain]=

https://www.meetgor.com/golang-web-url-parsing/
_c

:<<\_x
_f_url_parse http://a.b.c URL
echo ${URL[@]}
echo ${URL[port]}
_x

function _f_url_parse() {

#_debug_ns "_f_url_parse @: $@"

#[[ -n "$1" ]] || { _error '[[ -n "$1" ]]'; return 1; }

local ep=$1

(( $# < 2 )) && { _error '_f_url_parse (( $# < 2 ))'; return 1; }

local scheme user pass host port path query fragment
local url_no_scheme url_no_userpass url_no_hostport url_no_path userpass hostport


# ----------
if echo "$ep" | grep -q '://'; then

scheme=${ep/:\/\/*/}

scheme=${scheme:-https}  # default

scheme=$(echo "$scheme" | tr '[:upper:]' '[:lower:]')

url_no_scheme=${ep/*:\/\//}

else

url_no_scheme=$ep

fi


# ----------
if echo "$url_no_scheme" | grep -q '@'; then

userpass=${url_no_scheme%@*}

#pass=$(echo "$userpass" | grep ":" | cut -d":" -f2)
pass=$(echo "$userpass" | cut -s -d":" -f2)
if [ -n "$pass" ]; then
#  user=$(echo "$userpass" | grep ":" | cut -d":" -f1)
  user=$(echo "$userpass" | cut -s -d":" -f1)
else
  user="$userpass"
fi

url_no_userpass=${url_no_scheme/$userpass@/}

else

url_no_userpass=$url_no_scheme

fi


# ----------
hostport=${url_no_userpass%%/*}

if [[ -n "$hostport" ]]; then

host=$(echo "$hostport" | cut -d":" -f1)
host=${host,,}  # lowercase
#port=$(echo "$hostport" | grep ":" | cut -d":" -f2)
port=$(echo "$hostport" | cut -s -d":" -f2)

url_no_hostport=${url_no_userpass/$hostport/}

else

url_no_hostport=$url_no_userpass

fi

echo "$url_no_hostport" | grep -q '/' && {
path="${url_no_hostport%\?*}"  # cut off everything precedeing '/' and the '/'

url_no_path=${url_no_hostport/$path/}

echo "$url_no_path" | grep -q '?' && query=${url_no_path#*\?}
echo "$url_no_path" | grep -q '#' && fragment=${url_no_path#*\#}
}


# ----------
:<<\_c
i was getting this...
-bash: github.com: syntax error: invalid arithmetic operator (error token is ".com")
unset $2 fixed it
_c
unset $2

local ns
declare -g -A $2
declare -n ns=$2

#echo "2 host: $host \$(eval echo "$host")"

if [[ "$host" == *\$* ]]; then  # contains '$'?

ns[host]=$(eval echo $host)  # www${FDOM:+.$FDOM} -> ns[host]=www.runnable.org

else

ns[host]=$host  # ns[host]=www.runnable.org

fi

if [[ "${ns[host]}" =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then  # explicit ip?

# yes, it is an ip

ns[ip]=${ns[host]}
ns[sdom]=${ns[host]//./-}

else

# no, not an ip - assume it is a sdom

ns[sdom]=${host%%.*}  # www${FDOM:+.$FDOM} -> www

if [[ ${ns[host]} == *.* ]]; then
ns[domain]=${host#*.}  # www${FDOM:+.$FDOM} -> ${FDOM}
else
ns[domain]=""
fi

fi

if [[ -n "$port" ]]; then
ns[port]=$port
else
case $scheme in
http) ns[port]=80;;
https) ns[port]=443;;
esac
fi

[[ -n "$scheme" ]] && ns[scheme]=$scheme
[[ -n "$user" ]] && ns[user]=$user
[[ -n "$pass" ]] && ns[pass]=$pass
[[ -n "$path" ]] && ns[path]=$path
[[ -n "$query" ]] && ns[query]=$query
[[ -n "$fragment" ]] && ns[fragment]=$fragment

:<<\_c
# this fails...
test=now
declare -g -A ${test}_ARRAY
${test}_ARRAY[user]=me  # FAIL
echo ${${test}_ARRAY[user]}  # FAIL

eval ${test}_ARRAY[user]=me  # OK
eval echo ${${test}_ARRAY[user]}  # OK

# this works...
test=now
declare -g -A ${test}_ARRAY
declare -n ns=${test}_ARRAY
ns[user]=me  # OK
echo ${ns[user]}  # OK
_c
}
export -f _f_url_parse

:<<\_s
# avoid since it can polute stdout which is a problem with returning the address of metadata-server
(( DEBUG >= 1 )) && {
    echo "url: $url"
    echo "  scheme: $scheme"
    echo "    user: $user"
    echo "    pass: $pass"
    echo "    sdom: $sdom"
    echo "    domain: $domain"
    echo "    host: $host"
    echo "    ip: $ip"
    echo "    port: $port"
    echo "    path: $path"
    echo "   query: $query"
    echo "fragment: $fragment"
    echo ""
}
_s


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
:<<\__c
_cidr_parse <cidr> [namespace]  # -> 

cidr must end with .0/24
__c

:<<\_x
_cidr_parse 192.168.1.0/24 CIDR
set | grep CIDR_
_x

function _cidr_parse() {

#echo "_cidr_parse @: $@"

declare -g ${2:+${2}_}NETWORK=$(echo $1 | cut -d "/" -f 1)
declare -g ${2:+${2}_}PREFIX=$(echo $1 | cut -d "/" -f 2)
declare -g ${2:+${2}_}C=$(echo $1 | sed 's|.0/.*||')
}
export -f _cidr_parse


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
:<<\__c
physical interfaces must be associated with virtual interfaces
therefore, we need specs on both of those interfaces

_f_cidr_parse <hypervisor interface index><ifacekey>:<cidr> [array name]

<guest interface index><ifacekey>:<cidr>[?mode][#host interface name]
__c

:<<\_x
the information we need...
ifacekey
interface  # <- ${!<ifacekey>_IFACE}
cidr
mode

_f_cidr_parse "1)mnic:192.168.1.100/24?bridged#$MNIC_IFACE"
echo ${mnic[@]}

_f_cidr_parse "1)mnic:${MNIC_CIDR}?bridged#$MNIC_IFACE"

_f_cidr_parse "1)mnic:192.168.57.2/24?bridged#$MNIC_IFACE" MNIC
echo ${MNIC[@]}
set | grep MNIC

_f_cidr_parse "2)mq:192.168.57.2/24?intnet" INTNET
_x

function _f_cidr_parse() {

echo "_f_cidr_parse @: $@"

(( $# < 1 )) && { _error '_f_cidr_parse (( $# < 1 ))'; return 1; }

local index ifacekey remaining

remaining=$@

echo "$1" | grep -q ')' && {

index="${1%%)*}"  # everything before ')'

remaining="${1#*)}"  # everything after ')'

_debug "index: $index, remaining: $remaining"
}

# ----------
echo "$remaining" | grep -q ':' || { _error "Bad format: $remaining"; return 1; }

ifacekey=${remaining%%:*}  # everything before ':'

remaining="${remaining#*:}"  # everything after ':'

_debug "ifacekey: $ifacekey, remaining: $remaining"


# ----------
declare -g -A ${2:-$ifacekey}
declare -n ns=${2:-$ifacekey}

[[ -n "$index" ]] && ns[index]=$index
[[ -n "$ifacekey" ]] && ns[ifacekey]=$ifacekey
:<<\_x
echo ${ns[@]}
echo ${!ns[@]}
_x


# ----------
if echo "$remaining" | grep -q '?'; then
ns[cidr]=${remaining%%\?*}  # everything before '?'
remaining=${remaining#*\?}  # everything after '?'
elif echo "$remaining" | grep -q '#'; then
ns[cidr]=${remaining%%\#*}  # everything before '#'
remaining=${remaining#*\#}  # everything after '#'
else
ns[cidr]=$remaining
fi

_debug "ns[cidr]: ${ns[cidr]}, remaining: $remaining"

# ----------
if [[ "${ns[cidr]}" == *\$* ]]; then  # contains '$'?

_debug "ns[cidr]: ${ns[cidr]} \$(eval echo "${ns[cidr]}")"

ns[cidr]=$(eval echo ${ns[cidr]})  # ${MNIC_CIDR} -> ns[cidr]=1.2.3.4/24

fi

_debug "ns[cidr]: ${ns[cidr]}, remaining: $remaining"

# ----------
ns[ip]=$(echo "${ns[cidr]}" | cut -d"/" -f1)
ns[prefix]=$(echo "${ns[cidr]}" | cut -s -d"/" -f2)

:<<\_s
ns[c]=${ns[ip]%.*}  # www${FDOM:+.$FDOM} -> ${FDOM}
_s

[[ -n "$remaining" ]] && {
ns[mode]=${remaining%%\#*}  # everything before '#'
remaining=${remaining#*\#}  # everything after '#'
}

[[ -n "$remaining" ]] && {

ns[iface]="$remaining"

if [[ "${ns[iface]}" == *\$* ]]; then  # contains '$'?

_debug "ns[iface]=$(eval echo ${ns[iface]})"

ns[iface]=$(eval echo ${ns[iface]})  # ${MNIC_CIDR} -> ns[iface]="Intel(R) Wi-Fi 6 AX201 160MHz"

fi
}


# ----------
(( DEBUG > 0 )) && {
[[ "${ns[ip]}" =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]] || { _error "{ns[ip]}: ${ns[ip]}"; return 1; }
[[ "${ns[prefix]}" =~ ^[0-9]{1,3}$ ]] || { _error "{ns[prefix]}: ${ns[prefix]}"; return 1; }
}

}
export -f _f_cidr_parse


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
function _f_prompt() {

local line=$1

if [[ -n "$line" ]]; then

  echo $line

else

  while read line; do
     echo $line
  done

fi

read -p "Press Enter to continue." < /dev/tty

}
export -f _f_prompt


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
function _f_resolve() {  # -> stdout

name=$1

case $name in

WAN_IP)
curl -4sw '\n' "https://domains.google.com/checkip"
;;

*)
if [[ -v _HOSTS_ ]]; then

if [[ -v "$name" && -n "${!name}" ]]; then  # ${!hostmq}

  printf "%s" ${!name}

  true

else

  false
fi

else
. resolve.sh $name  # -> env
fi
;;

esac
}
export -f _f_resolve


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
:<<\_c
_picasso_install - $PROOF/picasso/_picasso_install/

if the file is relative to $BINV then run it
otherwise, download the file from https://${FQDN}/$dir/$file to $DESKSP_TMPDIR and then run it

a file that we need may be relative to various locations
1) /vagrant/...
2) $DESKSP/...
3) $BINV/...
4) http://bit.cafe/picasso/...

this function abstracts away the details of eeking out the target file

run script in this priority
1) local - $BINV/$dir/$file  # $BINV/${2%/*}/${2##*/}
2) remote - https://${FQDN}/$dir/$file  # https://${FQDN}/${2%/*}/${2##*/}
_c

:<<\_c
curl -s -o $DESKSP_TMPDIR/$dir/$file $BINV/$dir/$file && chmod a+x $DESKSP_TMPDIR/$dir/$file
#wget -qP $DESKSP_TMPDIR/$dir $BINV/$dir/$file && chmod a+x $DESKSP_TMPDIR/$dir/$file
_c

#echo xcsxzlcxlllsos
#DEBUG=2

function _picasso_install() {
_debug "_picasso_install $@"

#FDOM=${FDOM:-picasso.digital}

_debug "FDOM, $FDOM"

:<<\_c
if a file is not found locally, then fetch if from a trusted remote source

FDOM_PICASSO_PV_URL - trusted remote source hard-code here

this is not infalible - this function can be replaced by a modified one that fetches file from elsewhere
_c

_debug "FDOM_PICASSO_PV_URL: $FDOM_PICASSO_PV_URL"

local FDOM_PICASSO_PV_URL=${FDOM_PICASSO_PV_URL:-"https://www${FDOM:+.$FDOM}/.picasso/$PV"}

if [[ $1 == '.' || $1 == 'source' ]]; then
source_it=true
shift
else
source_it=false
fi

_debug "_picasso_install $@"

local dir="${1%/*}"
local file="${1##*/}"
dir=${dir#/}  # trim leading '/'
#dir=${dir%/}  # trim trailing '/'

_debug "BINV: $BINV, FDOM_PICASSO_PV_URL: $FDOM_PICASSO_PV_URL, dir: $dir, file: $file"

shift

if [[ -n "$BINV" && -e "$BINV/$dir/$file" ]]; then

_debug "offline: $BINV/$dir/$file"

if $source_it; then  # source the file?
_debug "source $BINV/$dir/$file"
. $BINV/$dir/$file
else
$BINV/$dir/$file
fi

#elif [[ "$BINV" == http://* ]]; then
elif [[ -n "$FDOM_PICASSO_PV_URL" ]]; then

[[ -n "$DESKSP_TMPDIR" && -d "$DESKSP_TMPDIR" ]] || export DESKSP_TMPDIR=$(mktemp -d -t picasso.XXXXXXXX)

[[ -f "$DESKSP_TMPDIR/picasso/$dir/$file" ]] || {

mkdir -p $DESKSP_TMPDIR/$dir

_debug "curl -s -o $DESKSP_TMPDIR/$dir $BINV/$dir/$file && chmod a+x $DESKSP_TMPDIR/$dir/$file"
if curl -s -o $DESKSP_TMPDIR/$dir/$file $FDOM_PICASSO_PV_URL/$dir/$file; then
chmod a+x $DESKSP_TMPDIR/$dir/$file
else
_error "curl -s -o $DESKSP_TMPDIR/$dir/$file $FDOM_PICASSO_PV_URL/$dir/$file"
return 1
fi
}

_debug "online: $DESKSP_TMPDIR/$dir/$file"

if $source_it; then  # source the file?
_debug "source $DESKSP_TMPDIR/$dir/$file"
. $DESKSP_TMPDIR/$dir/$file
else
$DESKSP_TMPDIR/$dir/$file
fi

else
_error "Unkown: BINV: $BINV"
return 1
fi

}
export -f _picasso_install


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
function _f_prompt() {
local line=$1
if [[ -n "$line" ]]; then
  echo $line
else
  while read line; do
     echo $line
  done
fi
read -p "Press Enter to continue." < /dev/tty
}
export -f _f_prompt


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
true
