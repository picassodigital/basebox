[[ -v DEBUG ]] && (( DEBUG >= 0 )) && printf "\e[1;38;5;228m>>> %s @: %s\e[0m\n" "${BASH_SOURCE[0]}" "${*@Q}"

:<<\_c
$PR/$PV/basebox/bin/dns.sh [dest] [KV_ENDPOINT]

configure the dns runtime

see: $PROOF/dns/
_c

:<<\_x
DNS_ENDPOINT=bind://$(_f_resolve MNIC_NAMESERVER1):$DNS_PORT
. dns.sh  # <- $DNS_ENDPOINT -> $PR/bin/{100-dns.env, 101-dns.fun}
_x

#echo sdsfdslfsf
#DEBUG=1

_debug_dns "dns.sh @: $@"

PR_BIN=${1:-$PR/bin}

DNS_ENDPOINT=${2:-$DNS_ENDPOINT}  # argument takes precidence

_debug_dns "DNS_ENDPOINT: $DNS_ENDPOINT"
[[ -n "$DNS_ENDPOINT" ]] || { _error '[[ -n "$DNS_ENDPOINT" ]]'; return 1; }
_f_url_parse $DNS_ENDPOINT DNS || { _error "_f_url_parse $DNS_ENDPOINT DNS"; return 1; }  # -> ${DNS[*]}


#if [[ "${DNS[host]}" =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then  # explicit ip?

#DNS[host]=${DNS[host]}
#fi

# DNS[host] must be an ip so that no dns server is required to resovle it
[[ -n "${DNS[host]}" ]] || _error_exit '[[ -n "${DNS[host]}" ]]'

_debug_dns "DNS[scheme]: ${DNS[scheme]}, DNS[host]: ${DNS[host]}, DNS[port]: ${DNS[port]}"


# ----------
case ${DNS[scheme]} in

bind)

cat > $PR_BIN/100-dns.env <<!
DNS_ENDPOINT=$DNS_ENDPOINT
_f_url_parse \$DNS_ENDPOINT DNS
!


#ln -sf $PR/$PV/bin/dns.fun $PR/bin/101-dns.fun  # autoload
ln -sf $(which dns.fun) $PR_BIN/101-dns.fun  # autoload

. $PR_BIN/100-dns.env
. $PR_BIN/101-dns.fun

true
;;

*) _error "Unknown DNS[scheme]: ${DNS[scheme]}"; false;;
esac


# ----------
#return $?
