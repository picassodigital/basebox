[[ -v DEBUG ]] && (( DEBUG >= 0 )) && printf "\e[1;38;5;228m>>> %s @: %s\e[0m\n" "${BASH_SOURCE[0]}" "${*@Q}"

:<<\_c
. guest-environment.sh

see: $PROOF/picasso-init/picasso-init.md

guests load their environment from two sources...
1) _f_sourcedir $PR/$PV/bin/ - core environment (basebox.tar/instance.tar)
2) _f_sourcedir $PR/bin/ - system provisioning environment
3) _f_sourcedir $PICASSO_HOME_DOT_PICASSO/bin/ - user provisioning environment
_c

:<<\_x
. guest-environment.sh

or, better yet because it only loads the file if necessary...

[[ -v _GENV_ ]] || . guest-environment.sh
_x

#echo "guest-bash-env000 _GENV_: $_GENV_"
#DEBUG=2
#PDEBUG=2


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
# source guest environment

echo beherenow
#[[ -v _GENV_ ]] || {

echo "guest-environment.sh _GENV_=true"

_GENV_=true  # we don't export this value. in bash, subshells do not inherit aliases which we may have defined in init.d. to include those aliases in our environment we must reload our environment in subshells.


# ----------
# create _debug function here just in case the function is used before calling it's big brother script that loads all debugging functions

# can we eliminate this...
#. $PR/$PV/bin/guest-debug.sh  # -> _debug()...


# ----------
function _f_sourcedir() {

#echo "$(ls -l $1)"

for script in $(/usr/bin/find $1 -maxdepth 1 -name '[0-9]*.*' \( -type l -o -type f \) | /usr/bin/sort); do

#echo "_f_sourcedir1 script: $script"

. $script || { echo ". $script"; sleep 10; exit 1; }

#echo "_f_sourcedir2 script: $script"

done

#echo "fini"
true
}


# ----------
#echo "vvs _f_sourcedir $PR/$PV/bin/"

_f_sourcedir $PR/$PV/bin/ || return 1  # ro - first, load disto functionality

_debug3 "wfee _f_sourcedir $PR/bin/"

_f_sourcedir $PR/bin/ || return 1  # rw - second, load system configuration

[[ -d $HOME/.picasso/bin ]] && {

_debug3 "sdf _f_sourcedir $HOME/.picasso/bin/"

_f_sourcedir $HOME/.picasso/bin/ || return 1  # third, load user configuration
}

_debug2 "guest-environment.sh done"

#}  # [[ -v _GENV_ ]]

true
