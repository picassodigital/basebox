:<<\_c
[usage]
. longopts.sh --test=TEST
. longopts.sh create --dproj=dhcp
. longopts.sh --dproj=dhcp create
. longopts.sh --adomain=picasso.digital --adomains="runnable.org bit.cafe"

OPTARGS="--one=1 --two=2 --three=3" longopts.sh --dproj=dhcp create

https://eklitzke.org/using-local-optind-with-bash-getopts
_c

:<<\_c
longopts - consumes all variables preceded with '--'
_c

#[[ -n "$@" ]] && {

#_debug3 "@ $@"
#_debug3 "OPTARGS $OPTARGS"

# marshal commandline arguments/values into the bash array: longopts
declare -A longopts=()

optspec=":v-:"  # ':' - leading colon turns on silent error reporting, 'v' first so DEBUG is set early, trailing ':' means to expect a parameter

other=()

if [[ -n "$@" ]]; then

# options are contained in $@

#_debug "longopts IFS: $IFS, @: $@"

for OPTARG in "$@"; do
#for OPTARG in "$*"; do

#echo "longopts OPTARG: $OPTARG"
  case "${OPTARG}" in
    --) break;;

    --*)
#echo "OPTARG:  ${OPTARG[*]}"
#echo "OPTIND:  ${OPTIND[*]}"

      OPTARG="${OPTARG:2}"  # remove '--'

# is there a value assigned to it '='
if [[ "$OPTARG" =~ '=' ]]; then

      val=${OPTARG#*=}  # split on '='
#      val="${OPTARG#*=}"  # split on '='
      opt=${OPTARG%=$val}
#echo "opt: $opt, val: $val"

      if [[ "$val" == "$OPTARG" ]]; then

        # no equal sign found

        _error "Undefined argument: '-${OPTARG}'"
        echo "do you mean: --${OPTARG}=<value>"
        break
      fi

#echo "--${OPTARG}" " " "${val}"

      longopts[${opt}]=${val}
else
      longopts[${OPTARG}]=""
fi
      ;;

    *)
#echo "other option OPTARG: $OPTARG"
other+=("$OPTARG")
      ;;

    esac

shift
done

#echo "asfwowru other: ${other[@]}"

set -- "${other[@]}"  # restore other parameters

# shift so that $@, $1, etc. refer to the non-option arguments
#shift "$((OPTIND-1))"
#OPTIND=1

else

for OPTARG in $OPTARGS; do

#_debug "longopts OPTARG: $OPTARG"

  case "${OPTARG}" in

    --) break;;

    --*)

      OPTARG="${OPTARG:2}"  # remove '--'

# is there a value assigned to it '='
if [[ "$OPTARG" =~ '=' ]]; then

      val=${OPTARG#*=}  # split on '='
      opt=${OPTARG%=$val}
#_debug3 "opt: $opt, val: $val"

      if [[ "$val" == "$OPTARG" ]]; then

        # no equal sign found

        _error "Undefined argument: '-${OPTARG}'"
        echo "do you mean: --${OPTARG}=<value>"
        break
      fi

#_debug "--${OPTARG}" " " "${val}"

      longopts[${opt}]=${val}
else
      longopts[${OPTARG}]=""
fi
      ;;

    *)
#_debug3 "other option OPTARG: $OPTARG"
other+=("$OPTARG")
      ;;

    esac

done

fi

#}
