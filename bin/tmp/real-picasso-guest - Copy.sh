[[ -v DEBUG ]] && (( DEBUG >= 0 )) && printf "\e[1;38;5;228m>>> %s @: %s\e[0m\n" "${BASH_SOURCE[0]}" "${*@Q}"

:<<\_c
. real-picasso-guest.sh

NB: this file is part of 'basebox.git' which gets embedded within each basebox - iow: changes to this file require a rebuild of the basebox

this script receives arguments that a provider may pass to it
ie: Vagrant passes through arguments that originate within the Vagrantfile
in addition, Vagrant also sets the environment variable PROVISIONER=<name of current provisioning script file>
note that arguments are received as pairs (--<name>=[value])

see: $NOTES/variables/PROVISIONER_ENV.md
proof: $DESKSP/proof/PROVISIONER_USER

[assumptions]
provisioner-env) file-system based endpoints require vboxsf

[usage]
. picasso-guest.sh $@
...
_c

:<<\_c
we load _debug functions just to support them between here and the bottom of the file when we run '. guest-environment.sh'
this would not be necessary if there were no debugging lines between here and '. guest-environment.sh'
_c
if (( DEBUG >= 1 )); then
. guest-debug.sh
else
# no-opts
function _debug() { :; }
function _debug2() { :; }
function _debug3() { :; }
function _debug_network() { :; }
fi

#echo "real-picasso-guest.sh @: $@ (USER: $USER, PROVISIONER_USER: $PROVISIONER_USER)"

:<<\_c
PROVISIONER_USER may come from env, argument, or config file
_c
:<<\_c
the environment value takes presidence over the cli argument - this is an exception!
normally a cli argument takes presidence over its equivalent environment value, but this is the opposite
cli arguments are passed through from Vagrant and we re-enter this script passing it the same arguments

ref: $DESKSP/proof/PROVISIONER_USER
_c
PROVISIONER_USER0=$PROVISIONER_USER

#(( DEBUG >= 1 )) && echo "real-picasso-guest.sh2 PROVISIONER_USER0: $PROVISIONER_USER0, USER: $USER"


# ----------
args=$@  # save a copy for PROVISIONER_USER

. longopts.sh  # <- $@(--*.*) -> longopts(), $@

for opt in ${!longopts[@]}; do
  val=${longopts[$opt]}
(( DEBUG >= 1 )) && echo "opt: $opt, val: $val"
  case $opt in

    provisioner-user)
#echo "sdfdsl provisioner-user opt: $opt, val: $val"
#[[ -z "$PROVISIONER_USER" ]] && PROVISIONER_USER=$val  # this is an exception - normally a cli argument take presidence over its equivalent environment value. this is the opposite, because we cannot cli argument is a passed through from Vagrant
PROVISIONER_USER=$val
    ;;

    provisioner-name)
      PROVISIONER_NAME=$val
    ;;

    provisioner-env)

:<<\_c
the first time we pass through here we may be 'root'
the second time we pass through here we may be $PROVISIONER_USER
therefore, we must use 'sudo cp' since we were 'root' when the file was originally created
TODO: fix this if possible
_c
      PROVISIONER_ENV=$val
(( DEBUG >= 1 )) && echo "Provisioning with environment values from $val"
      if [[ "$val" =~ (^| )http:// ]]; then
        sudo curl -s -o $PR/bin/020-provisioner.env $val
#        curl -s -o $PR/bin/020-provisioner.env $val
      elif [[ -f "$val" ]]; then
        sudo cp $val $PR/bin/020-provisioner.env
#        cp $val $PR/bin/020-provisioner.env
      fi

sudo chmod 644 $PR/bin/020-provisioner.env

. $PR/bin/020-provisioner.env  # -> $PROVISIONER_USER
# fall-through where guest-environment.sh will autoload $PR/bin/020-provisioner.env
    ;;

  esac
done

#(( DEBUG >= 1 )) && echo "real-picasso-guest.sh3 PROVISIONER_USER: $PROVISIONER_USER"

_debug "PROVISIONER_USER: $PROVISIONER_USER, PROVISIONER_USER0: $PROVISIONER_USER0"

PROVISIONER_USER=${PROVISIONER_USER0:-$PROVISIONER_USER}

[[ -f $PR/bin/020-provisioner.env ]] {

chown $PROVISIONER_USER:$PROVISIONER_USER $PR/bin/020-provisioner.env
}

#(( DEBUG >= 1 )) && echo "real-picasso-guest.sh4 PROVISIONER_USER: $PROVISIONER_USER"

[[ "$PROVISIONER_USER" != "$USER" ]] && {

_debug "PROVISIONER_USER: $PROVISIONER_USER, USER: $USER"

(( DEBUG >= 1 )) && echo "sudo -Hin -u $PROVISIONER_USER \"$0\" $args"

  sudo -Hin -u $PROVISIONER_USER "$0" $args; exit $?
}


# ----------
:<<\_c
$PR/bin/provider.env, 020-provisioner.env -> $PROVISIONER_USER
_c

#. guest-environment.sh "$@"  # <- longopts(), $@, _f_sourcedir() -> env, _debug...
. guest-environment.sh "$args"  # <- longopts(), $@, _f_sourcedir() -> env, _debug...

_info "Running provisioner '$PROVISIONER_NAME' as '$USER'"


# ----------
true
