[[ -v DEBUG ]] && (( DEBUG >= 0 )) && printf "\e[1;38;5;228m>>> %s @: %s\e[0m\n" "${BASH_SOURCE[0]}" "${*@Q}"

:<<\_c
. picasso-prov.sh

picasso-prov.sh and picasso-guest.sh are different in so far as pre-provisioners are passed different arguments than provisioners,
in addition, 'trigger' processes the provisioner argements and not the pre-provisioners arguments
therefore, firing trigger must be delayed until the first provisioner
iow: picasso-prov.sh does not fire the trigger, while picasso-provisioner does file the trigger
_c


# ----------
. longopts.sh  # <- $@ - marshal command-line arguments/values into the bash array: longopts

for opt in ${!longopts[@]}; do  # keys

val=${longopts[$opt]}
#echo "opt: $opt, val: $val"

case $opt in

debug)
  export DEBUG=$val
#echo "${BASH_SOURCE[0]}:${LINENO} DEBUG=$DEBUG"  # _debug does not yet exist
  ;;

debug-network)
  export DEBUG_NETWORK=$val
#echo "${BASH_SOURCE[0]}:${LINENO} DEBUG_NETWORK=$DEBUG_NETWORK"
  ;;

debug-firewall)
  export DEBUG_FIREWALL=$val
#echo "${BASH_SOURCE[0]}:${LINENO} DEBUG_FIREWALL=$DEBUG_FIREWALL"
  ;;

--) break;;

esac

done

:<<\_x
longopts[@] is an array - we can pass arrays amoung sourced script

ta=(1 2 3)
cat > ta.sh <<!
echo ${ta[@]}
!
. ta.sh
_x

:<<\_x
echo ${!longopts[@]}  # keys
echo ${longopts[@]}  # values
_x


# ----------
#echo "sfsfooo PROVISIONER_USER: $PROVISIONER_USER"

[[ -f "$PR/bin/provider.env" ]] && . $PR/bin/provider.env

#echo "sfsfooo2 PROVISIONER_USER: $PROVISIONER_USER"

. guest-environment.sh || { _error '. guest-environment.sh'; return 1; }

#echo "vsfslll PROVISIONER_USER: $PROVISIONER_USER"

[[ -n "$PROVISIONER_NAME" ]] && _info "Running provisioner '$PROVISIONER_NAME' as '$PROVISIONER_USER'"

true
