[[ -v DEBUG ]] && (( DEBUG >= 0 )) && printf "\e[1;38;5;228m>>> %s @: %s\e[0m\n" "${BASH_SOURCE[0]}" "${*@Q}"

:<<\_c
. $PR/$PV/basebox/bin/guest-debug.sh

there is a host version of this file and a guest version of this file

HOST:
. $PR/$PV/host/bin/host-debug.sh

GUEST:
. guest-debug.sh aka $PR/$PV/instance/bin/guest-debug.sh

each is meant to be the first file to automatically load in their respective environments

export DEBUG=-1  # silent & no testing
export DEBUG=0  # standard testing
export DEBUG=2  # high level debug testing
export DEBUG=2  # higher level debug testing
export DEBUG=3  # highest level debug testing

see: $PROOF/debug
_c

DEBUG_TRACE=${DEBUG_TRACE:-false}
PDEBUG_TRACE=${PDEBUG_TRACE:-false}


[[ -v PICASSO_LOG ]] && {
[[ -d /var/picasso ]] || {
sudo mkdir -p /var/picasso
sudo chown $USER:$USER /var/picasso
}
PICASSO_LOG_FILE=${PICASSO_LOG_FILE:-/var/picasso/cache.${BASHPID}}
#> $PICASSO_LOG_FILE  # create or purge
}

function _debug() {
(( DEBUG > 0 )) && {
local trace i j file

file=${BASH_SOURCE[$i]}
file=$(/usr/bin/basename ${file%% *})
trace="${file}($(( ${BASH_LINENO[0]} )))<-"

for (( i=0; i<$(( ${#FUNCNAME[@]} -1)); i++ )); do  # i=1: skip this function we are within

file=${BASH_SOURCE[$i]}
file=$(/usr/bin/basename ${file%% *})

if [[ "${FUNCNAME[$i]}" =~ source ]]; then
j=$(( i -1 ))
else
j=$i
fi

trace+="${file:+$file:}${FUNCNAME[$i]}($(( ${BASH_LINENO[$(( j ))]} )))<-"

done

trace=${trace%*<-}  # strip last '<-'

[[ -v PICASSO_LOG ]] && {
#logger -p local0.info "$@ - ${trace}"
echo "$@ - ${trace}" >> $PICASSO_LOG_FILE
}

[[ "${DEBUG_TRACE^^}" != 'TRUE' ]] && unset trace

  printf "\e[1;32m%b\e[0m \e[1;30m---> %s\e[0m\n" "$*" "${trace}" #]

  }
true
}
export -f _debug


function _debug2() {
(( DEBUG > 1 )) && {
local trace i j file

file=${BASH_SOURCE[$i]}
file=$(/usr/bin/basename ${file%% *})
trace="${file}($(( ${BASH_LINENO[0]} )))<-"

for (( i=0; i<$(( ${#FUNCNAME[@]} -1)); i++ )); do  # i=1: skip this function we are within

file=${BASH_SOURCE[$i]}
file=$(/usr/bin/basename ${file%% *})

if [[ "${FUNCNAME[$i]}" =~ source ]]; then
j=$(( i -1 ))
else
j=$i
fi

trace+="${file:+$file:}${FUNCNAME[$i]}($(( ${BASH_LINENO[$(( j ))]} )))<-"

done

trace=${trace%*<-}  # strip last '<-'

[[ -v PICASSO_LOG ]] && {
#logger -p local0.info "$@ - ${trace}"
echo "$@ - ${trace}" >> $PICASSO_LOG_FILE
}

[[ "${DEBUG_TRACE^^}" != 'TRUE' ]] && unset trace

  printf "\e[1;32m%b\e[0m \e[1;30m---> %s\e[0m\n" "$*" "${trace}" #]

  }
true
}
export -f _debug2


function _debug3() {
(( DEBUG > 2 )) && {
local trace i j file

file=${BASH_SOURCE[$i]}
file=$(/usr/bin/basename ${file%% *})
trace="${file}($(( ${BASH_LINENO[0]} )))<-"

for (( i=0; i<$(( ${#FUNCNAME[@]} -1)); i++ )); do  # i=1: skip this function we are within

file=${BASH_SOURCE[$i]}
file=$(/usr/bin/basename ${file%% *})

if [[ "${FUNCNAME[$i]}" =~ source ]]; then
j=$(( i -1 ))
else
j=$i
fi

trace+="${file:+$file:}${FUNCNAME[$i]}($(( ${BASH_LINENO[$(( j ))]} )))<-"

done

trace=${trace%*<-}  # strip last '<-'

[[ -v PICASSO_LOG ]] && {
#logger -p local0.info "$@ - ${trace}"
echo "$@ - ${trace}" >> $PICASSO_LOG_FILE
}

[[ "${DEBUG_TRACE^^}" != 'TRUE' ]] && unset trace

  printf "\e[1;32m%b\e[0m \e[1;30m---> %s\e[0m\n" "$*" "${trace}" #]

  }
true
}
export -f _debug3


function _debug_network() {
local trace i j file

file=${BASH_SOURCE[$i]}
file=$(/usr/bin/basename ${file%% *})
trace="${file}($(( ${BASH_LINENO[0]} )))<-"

for (( i=0; i<$(( ${#FUNCNAME[@]} -1)); i++ )); do  # i=1: skip this function we are within

file=${BASH_SOURCE[$i]}
file=$(/usr/bin/basename ${file%% *})

if [[ "${FUNCNAME[$i]}" =~ source ]]; then
j=$(( i -1 ))
else
j=$i
fi

trace+="${file:+$file:}${FUNCNAME[$i]}($(( ${BASH_LINENO[$(( j ))]} )))<-"

done

trace=${trace%*<-}  # strip last '<-'

[[ -v PICASSO_LOG ]] && {
#logger -p local0.info "$@ - ${trace}"
echo "$@ - ${trace}" >> $PICASSO_LOG_FILE
}

[[ "${DEBUG_TRACE^^}" != 'TRUE' ]] && unset trace

(( DEBUG >= 2 || DEBUG_NETWORK >= 1 )) && printf "\e[1;32m%b\e[0m \e[1;30m---> %s\e[0m\n" "$@" "$trace" #]

true
}
export -f _debug_network


function _debug_firewall() {
#(( DEBUG >= 2 || DEBUG_ENV >= 1 )) && printf "\e[1;32m%b\e[0m \e[1;30m---> %s\e[0m\n" "$@" "$trace" #]
local trace i j file

file=${BASH_SOURCE[$i]}
file=$(/usr/bin/basename ${file%% *})
trace="${file}($(( ${BASH_LINENO[0]} )))<-"

for (( i=0; i<$(( ${#FUNCNAME[@]} -1)); i++ )); do  # i=1: skip this function we are within

file=${BASH_SOURCE[$i]}
file=$(/usr/bin/basename ${file%% *})

if [[ "${FUNCNAME[$i]}" =~ source ]]; then
j=$(( i -1 ))
else
j=$i
fi

trace+="${file:+$file:}${FUNCNAME[$i]}($(( ${BASH_LINENO[$(( j ))]} )))<-"

done

trace=${trace%*<-}  # strip last '<-'

[[ -v PICASSO_LOG ]] && {
#logger -p local0.info "$@ - ${trace}"
echo "$@ - ${trace}" >> $PICASSO_LOG_FILE
}

[[ "${DEBUG_TRACE^^}" != 'TRUE' ]] && unset trace

(( DEBUG >= 2 || DEBUG_FIREWALL >= 1 )) && printf "\e[1;32m%b\e[0m \e[1;30m---> %s\e[0m\n" "$@" "$trace" #]

true
}
export -f _debug_firewall


function _debug_ns() {
local trace i j file

file=${BASH_SOURCE[$i]}
file=$(/usr/bin/basename ${file%% *})
trace="${file}($(( ${BASH_LINENO[0]} )))<-"

for (( i=0; i<$(( ${#FUNCNAME[@]} -1)); i++ )); do  # i=1: skip this function we are within

file=${BASH_SOURCE[$i]}
file=$(/usr/bin/basename ${file%% *})

if [[ "${FUNCNAME[$i]}" =~ source ]]; then
j=$(( i -1 ))
else
j=$i
fi

trace+="${file:+$file:}${FUNCNAME[$i]}($(( ${BASH_LINENO[$(( j ))]} )))<-"

done

trace=${trace%*<-}  # strip last '<-'

[[ -v PICASSO_LOG ]] && {
#logger -p local0.info "$@ - ${trace}"
echo "$@ - ${trace}" >> $PICASSO_LOG_FILE
}

[[ "${DEBUG_TRACE^^}" != 'TRUE' ]] && unset trace

(( DEBUG >= 2 || DEBUG_NS >= 1 )) && printf "\e[1;32m%b\e[0m \e[1;30m---> %s\e[0m\n" "$@" "$trace" #]

true
}
export -f _debug_ns


function _debug_cert() {
local trace i j file

file=${BASH_SOURCE[$i]}
file=$(/usr/bin/basename ${file%% *})
trace="${file}($(( ${BASH_LINENO[0]} )))<-"

for (( i=0; i<$(( ${#FUNCNAME[@]} -1)); i++ )); do  # i=1: skip this function we are within

file=${BASH_SOURCE[$i]}
file=$(/usr/bin/basename ${file%% *})

if [[ "${FUNCNAME[$i]}" =~ source ]]; then
j=$(( i -1 ))
else
j=$i
fi

trace+="${file:+$file:}${FUNCNAME[$i]}($(( ${BASH_LINENO[$(( j ))]} )))<-"

done

trace=${trace%*<-}  # strip last '<-'

[[ -v PICASSO_LOG ]] && {
#logger -p local0.info "$@ - ${trace}"
echo "$@ - ${trace}" >> $PICASSO_LOG_FILE
}

[[ "${DEBUG_TRACE^^}" != 'TRUE' ]] && unset trace

(( DEBUG >= 2 || DEBUG_CERT >= 1 )) && printf "\e[1;32m%b\e[0m \e[1;30m---> %s\e[0m\n" "$@" "$trace" #]

true
}
export -f _debug_cert

function _debug_dns() {
local trace i j file

file=${BASH_SOURCE[$i]}
file=$(/usr/bin/basename ${file%% *})
trace="${file}($(( ${BASH_LINENO[0]} )))<-"

for (( i=0; i<$(( ${#FUNCNAME[@]} -1)); i++ )); do  # i=1: skip this function we are within

file=${BASH_SOURCE[$i]}
file=$(/usr/bin/basename ${file%% *})

if [[ "${FUNCNAME[$i]}" =~ source ]]; then
j=$(( i -1 ))
else
j=$i
fi

trace+="${file:+$file:}${FUNCNAME[$i]}($(( ${BASH_LINENO[$(( j ))]} )))<-"

done

trace=${trace%*<-}  # strip last '<-'

[[ -v PICASSO_LOG ]] && {
#logger -p local0.info "$@ - ${trace}"
echo "$@ - ${trace}" >> $PICASSO_LOG_FILE
}

[[ "${DEBUG_TRACE^^}" != 'TRUE' ]] && unset trace

(( DEBUG >= 2 || DEBUG_DNS >= 1 )) && printf "\e[1;32m%b\e[0m \e[1;30m---> %s\e[0m\n" "$@" "$trace" #]

true
}
export -f _debug_dns

function _debug_hosts() {
local trace i j file

file=${BASH_SOURCE[$i]}
file=$(/usr/bin/basename ${file%% *})
trace="${file}($(( ${BASH_LINENO[0]} )))<-"

for (( i=0; i<$(( ${#FUNCNAME[@]} -1)); i++ )); do  # i=1: skip this function we are within

file=${BASH_SOURCE[$i]}
file=$(/usr/bin/basename ${file%% *})

if [[ "${FUNCNAME[$i]}" =~ source ]]; then
j=$(( i -1 ))
else
j=$i
fi

trace+="${file:+$file:}${FUNCNAME[$i]}($(( ${BASH_LINENO[$(( j ))]} )))<-"

done

trace=${trace%*<-}  # strip last '<-'

[[ -v PICASSO_LOG ]] && {
#logger -p local0.info "$@ - ${trace}"
echo "$@ - ${trace}" >> $PICASSO_LOG_FILE
}

[[ "${DEBUG_TRACE^^}" != 'TRUE' ]] && unset trace

(( DEBUG >= 2 || DEBUG_HOSTS >= 1 )) && printf "\e[1;32m%b\e[0m \e[1;30m---> %s\e[0m\n" "$@" "$trace" #]

true
}
export -f _debug_hosts


function _debug_pcopy() {
local trace i j file

file=${BASH_SOURCE[$i]}
file=$(/usr/bin/basename ${file%% *})
trace="${file}($(( ${BASH_LINENO[0]} )))<-"

for (( i=0; i<$(( ${#FUNCNAME[@]} -1)); i++ )); do  # i=1: skip this function we are within

file=${BASH_SOURCE[$i]}
file=$(/usr/bin/basename ${file%% *})

if [[ "${FUNCNAME[$i]}" =~ source ]]; then
j=$(( i -1 ))
else
j=$i
fi

trace+="${file:+$file:}${FUNCNAME[$i]}($(( ${BASH_LINENO[$(( j ))]} )))<-"

done

trace=${trace%*<-}  # strip last '<-'

[[ -v PICASSO_LOG ]] && {
#logger -p local0.info "$@ - ${trace}"
echo "$@ - ${trace}" >> $PICASSO_LOG_FILE
}

[[ "${DEBUG_TRACE^^}" != 'TRUE' ]] && unset trace

(( DEBUG >= 2 || DEBUG_PCOPY >= 1 )) && printf "\e[1;32m%b\e[0m \e[1;30m---> %s\e[0m\n" "$@" "$trace" #]

true
}
export -f _debug_pcopy


function _debug_kv() {
local trace i j file

file=${BASH_SOURCE[$i]}
file=$(/usr/bin/basename ${file%% *})
trace="${file}($(( ${BASH_LINENO[0]} )))<-"

for (( i=0; i<$(( ${#FUNCNAME[@]} -1)); i++ )); do  # i=1: skip this function we are within

file=${BASH_SOURCE[$i]}
file=$(/usr/bin/basename ${file%% *})

if [[ "${FUNCNAME[$i]}" =~ source ]]; then
j=$(( i -1 ))
else
j=$i
fi

trace+="${file:+$file:}${FUNCNAME[$i]}($(( ${BASH_LINENO[$(( j ))]} )))<-"

done

trace=${trace%*<-}  # strip last '<-'

[[ -v PICASSO_LOG ]] && {
#logger -p local0.info "$@ - ${trace}"
echo "$@ - ${trace}" >> $PICASSO_LOG_FILE
}

[[ "${DEBUG_TRACE^^}" != 'TRUE' ]] && unset trace

(( DEBUG >= 2 || DEBUG_KV >= 1 )) && printf "\e[1;32m%b\e[0m \e[1;30m---> %s\e[0m\n" "$@" "$trace" #]

true
}
export -f _debug_kv


function _debug_env() {
local trace i j file

file=${BASH_SOURCE[$i]}
file=$(/usr/bin/basename ${file%% *})
trace="${file}($(( ${BASH_LINENO[0]} )))<-"

for (( i=0; i<$(( ${#FUNCNAME[@]} -1)); i++ )); do  # i=1: skip this function we are within

file=${BASH_SOURCE[$i]}
file=$(/usr/bin/basename ${file%% *})

if [[ "${FUNCNAME[$i]}" =~ source ]]; then
j=$(( i -1 ))
else
j=$i
fi

trace+="${file:+$file:}${FUNCNAME[$i]}($(( ${BASH_LINENO[$(( j ))]} )))<-"

done

trace=${trace%*<-}  # strip last '<-'

[[ -v PICASSO_LOG ]] && {
#logger -p local0.info "$@ - ${trace}"
echo "$@ - ${trace}" >> $PICASSO_LOG_FILE
}

[[ "${DEBUG_TRACE^^}" != 'TRUE' ]] && unset trace

(( DEBUG >= 2 || DEBUG_ENV >= 1 )) && printf "\e[1;32m%b\e[0m \e[1;30m---> %s\e[0m\n" "$@" "$trace" #]

true
}
export -f _debug_env


function _debug_mounts() {
local trace i j file

file=${BASH_SOURCE[$i]}
file=$(/usr/bin/basename ${file%% *})
trace="${file}($(( ${BASH_LINENO[0]} )))<-"

for (( i=0; i<$(( ${#FUNCNAME[@]} -1)); i++ )); do  # i=1: skip this function we are within

file=${BASH_SOURCE[$i]}
file=$(/usr/bin/basename ${file%% *})

if [[ "${FUNCNAME[$i]}" =~ source ]]; then
j=$(( i -1 ))
else
j=$i
fi

trace+="${file:+$file:}${FUNCNAME[$i]}($(( ${BASH_LINENO[$(( j ))]} )))<-"

done

trace=${trace%*<-}  # strip last '<-'

[[ -v PICASSO_LOG ]] && {
#logger -p local0.info "$@ - ${trace}"
echo "$@ - ${trace}" >> $PICASSO_LOG_FILE
}

[[ "${DEBUG_TRACE^^}" != 'TRUE' ]] && unset trace

(( DEBUG >= 2 || DEBUG_MOUNTS >= 1 )) && printf "\e[1;32m%b\e[0m \e[1;30m---> %s\e[0m\n" "$@" "$trace" #]

true
}
export -f _debug_mounts


# ----------
:<<\_c
syslog severity levels...
0	emerg	system is unusable
1	alert	action must be taken immediately
2	crit	critical conditions
3	error	error conditions
4	warning	warning conditions
5	notice	normal but significant condition
6	info	informational messages
7	debug	debug-level messages
_c

function _error() {
local trace i j file

file=${BASH_SOURCE[$i]}
file=$(/usr/bin/basename ${file%% *})
trace="${file}($(( ${BASH_LINENO[0]} )))<-"

for (( i=0; i<$(( ${#FUNCNAME[@]} -1)); i++ )); do  # i=1: skip this function we are within

file=${BASH_SOURCE[$i]}
file=$(/usr/bin/basename ${file%% *})

if [[ "${FUNCNAME[$i]}" =~ source ]]; then
j=$(( i -1 ))
else
j=$i
fi

trace+="${file:+$file:}${FUNCNAME[$i]}($(( ${BASH_LINENO[$(( j ))]} )))<-"

done

trace=${trace%*<-}  # strip last '<-'

[[ -v PICASSO_LOG ]] && {
#logger -p local0.error "$@ - ${trace}"
echo "ERROR: $@ - ${trace}" >> $PICASSO_LOG_FILE
}

[[ "${DEBUG_TRACE^^}" != 'TRUE' ]] && unset trace

    printf "\e[1;41mERROR: %b\e[0m \e[1;30m---> %s\e[0m\n" "$@" "$trace" #]

}
export -f _error


# ----------
function _error_exit() {
local trace i j file

file=${BASH_SOURCE[$i]}
file=$(/usr/bin/basename ${file%% *})
trace="${file}($(( ${BASH_LINENO[0]} )))<-"

for (( i=0; i<$(( ${#FUNCNAME[@]} -1)); i++ )); do  # i=1: skip this function we are within

file=${BASH_SOURCE[$i]}
file=$(/usr/bin/basename ${file%% *})

if [[ "${FUNCNAME[$i]}" =~ source ]]; then
j=$(( i -1 ))
else
j=$i
fi

trace+="${file:+$file:}${FUNCNAME[$i]}($(( ${BASH_LINENO[$(( j ))]} )))<-"

done

trace=${trace%*<-}  # strip last '<-'

[[ -v PICASSO_LOG ]] && {
#logger -p local0.crit "$@ - ${trace}"
echo "ERROR CRITICAL: $@ - ${trace}" >> $PICASSO_LOG_FILE
}

[[ "${DEBUG_TRACE^^}" != 'TRUE' ]] && unset trace

    >&2 printf "\e[1;31mERROR CRITICAL: %b\e[0m \e[1;30m---> %s\e[0m\n" "$*" "$trace" #]

exit 1
}
export -f _error_exit


# ----------
function _info() {

(( DEBUG < 0 )) && return 0

printf "\e[0;32mINFO: %b \e[0m\n" "$*" #]

[[ -v PICASSO_LOG ]] && {
#logger -p local0.info "$@"
echo "INFO: $@" >> $PICASSO_LOG_FILE
}

}
export -f _info


# ----------
function _warn() {

printf "\e[1;31mWARN: $@ \e[0m\n" #]

[[ -v PICASSO_LOG ]] && {
#logger -p local0.warn "$@"
echo "WARN: $@" >> $PICASSO_LOG_FILE
}

}
export -f _warn


# ----------
function _f_prompt() {

local line=$1

if [[ -n "$line" ]]; then

  echo $line

else

  while read line; do
     echo $line
  done

fi

read -p "Press Enter to continue." < /dev/tty

}
export -f _f_prompt


# ----------
true
