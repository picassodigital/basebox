#!/usr/bin/env bash
[[ -v DEBUG ]] && (( DEBUG >= 0 )) && printf "\e[1;38;5;228m>>> %s @: %s\e[0m\n" "${BASH_SOURCE[0]}" "${*@Q}"

:<<\_c
$PR/$PV/bin/trigger.sh - built into each basebox

consider not building this into service baseboxes

1) $PR/bin/picasso-guest.sh
2) trigger.sh
3) real-picasso-guest.sh

building of the basebox does this...

$DESKSP/basebox/vagrant/0.0.2/post-provisioner.sh {

[[ -e $PR/bin/picasso-guest.sh ]] || \
ln -s $PR/$PV/bin/trigger.sh $PR/bin/picasso-guest.sh
}

$PR/bin/picasso-guest.sh (aka $PR/$PV/bin/trigger.sh) {

ln -sf $PR/$PV/bin/real-picasso-guest.sh $PR/bin/picasso-guest.sh
}

subsequent times when picasso-guest.sh is run real-picasso-guest.sh is run
_c

:<<\_c
we load _debug functions just to support them between here and the bottom of the file when we run '. guest-environment.sh'
this would not be necessary if there were no debugging lines between here and '. guest-environment.sh'
_c

. guest-environment.sh  # <- $PR/bin/xx-*.*, $PR/$PV/bin/xx-*.*

#DEBUG=3

_debug "trigger2 PROVISIONER_USER: $PROVISIONER_USER"
_debug "$(whoami)"


# ----------
:<<\_c
picasso-guest.sh is designed to re-enter its calling script if [[ $PROVISIONER_USER != root ]]
we do not re-enter this script due to the symlink change above 
the re-entry overlays the current shell with a new one and, most importantly, to terminate execution within the current shell
iow: if [[ $PROVISIONER_USER != root ]] this script will not execute anything below the sourcing of picasso-guest.sh
the provisioner is the target to the re-entery
_c

:<<\_c
we need to force PROVISIONER_USER=root to avoid re-entry and allow this script to complete...
PROVISIONER_USER=root . $PR/bin/picasso-guest.sh

however; we have this built-in design...
picasso-guest.sh {
$PR/bin/provider.env -> $PROVISIONER_USER
020-provisioner.env -> $PROVISIONER_USER
}

therefore, we need to source picasso-guest.sh before creating either of those files
_c

:<<\_c
NB: this file is incorporated into each basebox - iow: changes to this file require a rebuild of the basebox

this script receives arguments that Vagrant passes to it that originate within the Vagrantfile
in addition, Vagrant also sets the environment variable PROVISIONER=<name of current provisioning script file>
note that arguments are received as pairs (--<name>=[value])

[assumptions]
provisioner-env) may require vboxsf

[usage]
. guest-environment.sh #$@
...
_c


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
:<<\_c
only the first provisioner processes this...
iow: chained provisioners never get here
_c

:<<\_s
# $PR/$PV/bin/picasso-prov.sh has already done this...
[[ -n "$@" ]] && . longopts.sh
_s

_debug3 "longopts: ${longopts[@]}"
#other=()

for opt in ${!longopts[@]}; do  # keys
val=${longopts[$opt]}
_debug3 "opt: $opt, val: $val"

case $opt in

provisioner-name)
PROVISIONER_NAME=$val
;;

#provisioner-user)
#PROVISIONER_USER=$val
#;;

provisioner-env)

# provisioner.sh scripts have no knowledge that run-once trigger script preceded it
# they simply have to follow convention of the trigger's results like the existence of $PR/bin/020-provisioner.env

#echo "Trigger sdfsfds"
_info "Loading provisioner-env from $val"

  if [[ "$val" =~ (^| )http:// ]]; then

    curl -s -o $PR/bin/020-provisioner.env $val

  elif [[ -f "$val" ]]; then

    cp "$val" $PR/bin/020-provisioner.env

  else
    _error_exit "Environment file not found: $val"
  fi

chmod 644 $PR/bin/020-provisioner.env

#. $PR/bin/020-provisioner.env  # -> $PROVISIONER_USER

;;

esac

done
:<<\_j

*)
_debug3 "sfsso opt: $opt"
if [[ -n "$val" ]]; then
other+=("--${opt}=$val")
else
other+=("--${opt}")
fi
;;

#echo "sfsso other: ${other[@]}"
set -- "${other[@]}" # restore other parameters
_j

_debug2 "sdzzozswwls HOSTNAME: $HOSTNAME, ROOT_DOMAIN: $ROOT_DOMAIN, PROVIDER_ENV: $PROVIDER_ENV"


# ----------
_debug3 "$(ls -l $PR/bin/provider.env)"

[[ -f "$PR/bin/provider.env" ]] || {
#if [[ -f "$PR/bin/provider.env" ]]; then

#. $PR/bin/provider.env  # -> $READONLY_MNT, $PICASSO_GIT_ENDPOINT

#else

# provider.env was not specified as an argument so assume cloudinit functionality

_debug "assume cloudinit"

curl -s -o $PR/bin/provider.env http://169.254.169.253:8080/$HOSTNAME/provider.env && {

cat >> $PR/bin/provider.env <<!
METADATA_SOURCE=cloudinit
LOCAL_METADATA_URL=http://169.254.169.253:8080/$HOSTNAME
!

chmod 644 $PR/bin/provider.env
}

#fi
}


# ----------
# is this necessary???
#:<<\_j
[[ -f "$PR/bin/provider.env" ]] && {

_debug "$(<$PR/bin/provider.env)"

:<<\_c
secure
_c
#chmod 600 $PR/bin/provider.env

. $PR/bin/provider.env  # -> $PROVISIONER_ENV
}
#_j


# ----------
_debug2 "PROVISIONER_ENV: $PROVISIONER_ENV, METADATA_SOURCE: $METADATA_SOURCE, LOCAL_METADATA_URL: $LOCAL_METADATA_URL"

:<<\_c
$PR/bin/provider.env is already loaded and it may declare $PROVISIONER_ENV

the origin of PROVISIONER_ENV may be cloudinit script or a virtualbox argument

NB: we append to any existing file
_c

:<<\_s
[[ -n "$PROVISIONER_ENV" ]] && {

case "$METADATA_SOURCE" in

metafile)
_info "Loading metadata from /vagrant/$PROVISIONER_ENV"
cat /vagrant/$PROVISIONER_ENV >> $PR/bin/020-provisioner.env
;;

cloudinit)
_info "Loading metadata from $LOCAL_METADATA_URL/$PROVISIONER_ENV"
curl -s $LOCAL_METADATA_URL/$PROVISIONER_ENV >> $PR/bin/020-provisioner.env
;;

*) _error "Unknown METADATA_SOURCE: $METADATA_SOURCE";;

esac

_debug2 "$(ls -l $PR/bin/020-provisioner.env)"
_debug3 "$(cat $PR/bin/020-provisioner.env)"
}
_s

#:<<\_s
#defer loading
[[ -f "$PR/bin/020-provisioner.env" ]] && {

echo swwowroworwowrowrwr
. $PR/bin/020-provisioner.env
}
#_s

_debug "trigger4 PROVISIONER_USER: $PROVISIONER_USER"

# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
_debug3 "_run_once_on_entry"

_debug "READONLY_MNT: $READONLY_MNT"
_debug "PICASSO_GIT_ENDPOINT: $PICASSO_GIT_ENDPOINT"

# if READONLY_MNT is defined, prov-cifs.sh will mount it

if [[ -n "$READONLY_MNT" ]]; then

_debug2 "xcvbsdwwewyisddsz READONLY_MNT: $READONLY_MNT"
_debug3 "$(ls -l $READONLY_MNT)"
_debug3 "$(ls -l $PR/v)"

:<<\_c
if the directory already exists, then it has already been initialized
it may already have been initialized within the basebox
or, it may already have been initialized by earlier chained provisioning

the question of whether or not to overwrite the existing directory arises
if we don't overwrite, then a new basebox must be used that contains the desired files?
if we do overwrite, then how do we do so only once and not by each chained provisioner?

philosophy...
that done within the basebox remains, and therefore, during basebox building, we don't initialize these directories
iow: $PR/core is pre-initialized within the basebox and does not get updated - use the basebox with the 'guest' files you need, because at provision time you cammot modify 'guest' files
other modules are installed on a first come, first served, basis
first come first served makes sense during provisioning, since the files should not change mid-provisioning
_c
[[ -d "$PR/$PV/instance" ]] || {
mkdir -p $PR/$PV/instance
cp -fr $READONLY_MNT/$PV/instance/* $PR/$PV/instance/
}

:<<\_s
[[ -d "$PR/$PV/custom" ]] || {
mkdir -p $PR/$PV/custom
cp -fr $READONLY_MNT/$PV/custom/* $PR/$PV/custom/
}
_s

elif [[ -n "$PICASSO_GIT_ENDPOINT" ]]; then

_debug2 "xcvbsdwewwyisddsz PICASSO_GIT_ENDPOINT: $PICASSO_GIT_ENDPOINT"

#_install git

:<<\_c
basebox.git is statically cloned into each prebuilt basebox
instance.git is dynamically cloned into each provisioned instance
_c

:<<\_s
this has already been done in $DESKSP/basebox/*/*/pre-provisioner.sh...

#[[ -d "$PR/$PV/basebox" ]] || git clone $PICASSO_GIT_ENDPOINT/basebox.git $PR/$PV/basebox
[[ -d "$PR/$PV/basebox" ]] || { mkdir -p $PR/$PV/basebox; curl -s $PICASSO_GIT_ENDPOINT/tar/raw/master/basebox.tar.gz | tar -xz -C $PR/$PV/; }
_s


[[ -d "$PR/$PV/instance" ]] || {

git clone $PICASSO_GIT_ENDPOINT/instance.git $PR/$PV/instance
#git clone $PICASSO_GIT_ENDPOINT/$PV/instance.git $PR/$PV/instance

cp $PR/$PV/instance/bin/* $PR/$PV/bin/  # overlay bin/* to save having to add each repo's bin path to $PATH
}

:<<\_x
[[ -f "$PR/$PV/instance.txt" ]] || {
curl -s $PICASSO_GIT_ENDPOINT/tar/raw/master/instance.tar.gz | tar -xz -C $PR/$PV/
}
_x
else
_error "Unknown PICASSO_GIT_ENDPOINT: $PICASSO_GIT_ENDPOINT"
fi

_debug3 "$(ls -la $PR/v)"


# this is deferred to last...

:<<\_c
only run this script file once
$PR/bin/picasso-guest.sh linked to $PR/$PV/bin/real-picasso-guest.sh indicates the script has run
_c
ln -sf $PR/$PV/bin/real-picasso-guest.sh $PR/bin/picasso-guest.sh

_debug "trigger5 PROVISIONER_USER: $PROVISIONER_USER"

. real-picasso-guest.sh

_debug "trigger6 PROVISIONER_USER: $PROVISIONER_USER"
