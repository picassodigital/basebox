:<<\_c
. iptables.fun

MNIC and XNIC interfaces are handled differently
MNIC is assumed to be the management network - source and destination addresses are assumed to be $MNIC_CIDR
XNIC is assumed to be the external network - source and destination addresses are assumed to be 0.0.0.0/0
_c

function _debug_firewall() {
  (( DEBUG_FIREWALL > 0 )) && {
    local l=${#BASH_LINENO[@]}
    local f=${BASH_SOURCE[1]}
    f=$(basename ${f:-#})
    printf "\e[1;32m--->%b:%s %s\e[0m\n" "$f" "${BASH_LINENO[-$l]}" "$*"
  } || true
}
export -f _debug_firewall

#declare -p NEMOS &>/dev/null || nemo_env  # -> NEMOS[*]
#echo hghgsho
#declare -p NEMOS
#declare -p NEMO_MNIC
#cat $PR/bin/010-system.sh


# ---------- ---------- ---------- ---------- ----------
:<<\_c
copies all rules to $PR/iptables and applies the rule ephemerally
using this indirect function, persistent iptables changes can then be made centrally by applying the rules accumulated in $PR/iptables
# $1:rule
_c

function _iptables() {

echo "$1" >> $PR/iptables

iptables $1
}
export -f _iptables


# ---------- ---------- ---------- ---------- ----------
:<<\_s
alias _tcp_input_accept="sudo iptables -A INPUT -p tcp -j ACCEPT "
alias _tcp_output_accept="sudo iptables -A OUTPUT -p tcp -j ACCEPT "
alias _udp_input_accept="sudo iptables -A INPUT -p udp -j ACCEPT "
alias _udp_output_accept="sudo iptables -A OUTPUT -p udp -j ACCEPT "
alias _tcp_input_drop="sudo iptables -A INPUT -p tcp -j DROP "
alias _tcp_output_drop="sudo iptables -A OUTPUT -p tcp -j DROP "
alias _udp_input_drop="sudo iptables -A INPUT -p udp -j DROP "
alias _udp_output_drop="sudo iptables -A OUTPUT -p udp -j DROP "
_s


INPUT_CHAIN=${INPUT_CHAIN:-PICASSO_INPUT}
OUTPUT_CHAIN=${OUTPUT_CHAIN:-PICASSO_OUTPUT}


#alias _tcp_in_accept="sudo iptables -A ${INPUT_CHAIN:-INPUT} -p tcp -j ACCEPT "
function _tcp_in_accept() {
#(( DEBUG >= 1 )) && echo "${_TOP_:-$0}:$LINENO iptables -A ${INPUT_CHAIN:-INPUT} -p tcp -j ACCEPT $@ "
sudo iptables -A ${INPUT_CHAIN:-INPUT} -p tcp -j ACCEPT "$@"
}
export -f _tcp_in_accept

#alias _tcp_out_accept="sudo iptables -A ${OUTPUT_CHAIN:-OUTPUT} -p tcp -j ACCEPT "
function _tcp_out_accept() {
#(( DEBUG >= 1 )) && echo "${_TOP_:-$0}:$LINENO iptables -A ${OUTPUT_CHAIN:-OUTPUT} -p tcp -j ACCEPT $@ "
sudo iptables -A ${OUTPUT_CHAIN:-OUTPUT} -p tcp -j ACCEPT "$@"
}
export -f _tcp_out_accept

alias _udp_in_accept="sudo iptables -A ${INPUT_CHAIN:-INPUT} -p udp -j ACCEPT "
alias _udp_out_accept="sudo iptables -A ${OUTPUT_CHAIN:-OUTPUT} -p udp -j ACCEPT "
alias _tcp_in_drop="sudo iptables -A ${INPUT_CHAIN:-INPUT} -p tcp -j DROP "
alias _tcp_out_drop="sudo iptables -A ${OUTPUT_CHAIN:-OUTPUT} -p tcp -j DROP "
alias _udp_in_drop="sudo iptables -A ${INPUT_CHAIN:-INPUT} -p udp -j DROP "
alias _udp_out_drop="sudo iptables -A ${OUTPUT_CHAIN:-OUTPUT} -p udp -j DROP "


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
:<<\_c
_open_server_firewall <interface> <port/s> <comment> <type>
port/s - local destination port/s & local source port/s
type - UDP

workflow:
client(any port) -> server(specific port) -> client(any port)
_c
:<<\_c
interface...
127.0.0.1|localhost - do not open firewall
MNIC|$MNIC_IP - open firewall on $MNIC_IFACE ${MANAGEMENT_C}.0/$MANAGEMENT_PREFIX only
XNIC|$XNIC_IP - open firewall on "$XNIC_IFACE" $XNIC_CIDR only
NICSA - open firewall on array of interfaces
FNICSA - open firewall on array of interfaces
_c
:<<\_x
. iptables.fun
_open_server_firewall MNIC '53' "dns" UDP
-A PICASSO_INPUT -s 192.168.1.0/24 -i enp0s8 -p udp -m comment --comment "/mnt/r/picasso/provisioner.sh:67" -m multiport --dports 53 -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment -server -j ACCEPT
-A PICASSO_OUTPUT -d 192.168.1.0/24 -o enp0s8 -p udp -m comment --comment "/mnt/r/picasso/provisioner.sh:68" -m multiport --sports 53 -m conntrack --ctstate ESTABLISHED -m comment --comment -server -j ACCEPT
_x

# _open_server_firewall MNIC,XNIC $PORT 'deno'

function _open_server_firewall() {
_debug3 "_open_server_firewall $@"
#echo ghghhgso
#declare -p NEMOS
#declare -p NEMO_MNIC
#cat $PR/bin/010-system.sh

# synonymous...
case $1 in
MNIC_IP) local interfaces=MNIC;;
XNIC_IP) local interfaces=XNIC;;
*) local interfaces=$1;;
esac

local ports=$2
local comment=$3

if [[ -n "$4" ]]; then
tcp=false
echo $4 | grep -q -i 'tcp' && tcp=true
udp=false
echo $4 | grep -q -i 'udp' && udp=true
else
tcp=true  # default is tcp only
udp=false
fi
#_debug "wfow7fs90fwjf interfaces: $interfaces, ports: $ports, comment: $comment, udp: $udp, tcp: $tcp"

for nemonic in ${interfaces//,/ }; do

_debug "sdf nemonic: $nemonic"

[[ -v NEMOS[$nemonic] ]] || {

_warn "sfs Missing NEMOS[$nemonic]"
continue
}

_debug2 "$(declare -p NEMOS)"
_debug2 "NEMOS[$nemonic]: ${NEMOS[$nemonic]}"
_debug2 "$(declare -p NEMO_MNIC)"

declare -n nemo=${NEMOS[$nemonic]}  # nemo=NEMO_MNIC[]

_debug2 "$(declare -p nemo)"

[[ -v nemo[iface] ]] || {

_warn "llj Missing nemo[iface]"
continue
}

local iface=${nemo[iface]}
local cidr=${nemo[cidr]}

_debug_firewall "$nemonic - iface: $iface, cidr: $cidr"

case $nemonic in

127.0.0.1|localhost)
:  # do not expose anything
#_tcp_in_accept -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-server"
#_tcp_out_accept -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-server"
;;

MNIC)

if $udp; then
_udp_in_accept -i "$iface" -s $cidr -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-server"
_udp_out_accept -o "$iface" -d $cidr -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-server"
fi
if $tcp; then
_tcp_in_accept -i "$iface" -s $cidr -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-server"
_tcp_out_accept -o "$iface" -d $cidr -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-server"
#_tcp_in_accept -i "$iface" -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-server"
#_tcp_out_accept -o "$iface" -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-server"
fi
;;

XNIC)

if $udp; then
#_udp_in_accept -i "$iface" -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-server"
#_udp_out_accept -o "$iface" -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-server"
#_udp_in_accept -i "$iface" -s $cidr -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-server"
#_udp_out_accept -o "$iface" -d $cidr -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-server"
_udp_in_accept -i "$iface" -s 0.0.0.0/0 -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-server"
_udp_out_accept -o "$iface" -d 0.0.0.0/0 -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-server"
fi
if $tcp; then
#echo "_tcp_in_accept -i "$iface" -s $MNIC_CIDR -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment \"${comment}-server\""
#_tcp_in_accept -i "$iface" -s $cidr -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-server"
#_tcp_out_accept -o "$iface" -d $cidr -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-server"
#_tcp_in_accept -i "$iface" -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-server"
#_tcp_out_accept -o "$iface" -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-server"
#_tcp_in_accept -i "$iface" -s $cidr -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-server"
#_tcp_out_accept -o "$iface" -d $cidr -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-server"
_tcp_in_accept -i "$iface" -s 0.0.0.0/0 -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-server"
_tcp_out_accept -o "$iface" -d 0.0.0.0/0 -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-server"
fi
;;

SNIC|TNIC)

if $udp; then
_udp_in_accept -i "$iface" -s $cidr -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-server"
_udp_out_accept -o "$iface" -d $cidr -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-server"
fi
if $tcp; then
_tcp_in_accept -i "$iface" -s $cidr -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-server"
_tcp_out_accept -o "$iface" -d $cidr -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-server"
fi
;;

NICSA)

prev_IFACE=

for nemonic in "${NICSA[@]}"; do

# remove leading whitespace characters
nemonic="${nemonic#"${nemonic%%[![:space:]]*}"}"
# remove trailing whitespace characters
nemonic="${nemonic%"${nemonic##*[![:space:]]}"}" 

[[ -v NEMOS[$nemonic] ]] || {

_warn "Missing NEMOS[$nemonic]"
continue
}

declare -n nemo=${NEMOS[$nemonic]}

[[ -v nemo[iface] ]] || {

_warn "Missing nemo[iface]"
continue
}

local iface=${nemo[iface]}

#nic_IFACE=${nemonic}_IFACE
#[[ -n "$prev_IFACE" && "${nic_IFACE}" == "${prev_IFACE}" ]] && continue
#[[ -n "$prev_IFACE" && "$iface" == "${prev_IFACE}" ]] && continue
[[ "$iface" == "${prev_IFACE}" ]] && continue

if [[ "$nemonic" == 'XNIC' ]]; then
if $udp; then
#_udp_in_accept -i "${!nic_IFACE}" -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-server"
#_udp_out_accept -o "${!nic_IFACE}" -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-server"
_udp_in_accept -i "$iface" -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-server"
_udp_out_accept -o "$iface" -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-server"
fi
if $tcp; then
#_tcp_in_accept -i "${!nic_IFACE}" -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-server"
#_tcp_out_accept -o "${!nic_IFACE}" -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-server"
_tcp_in_accept -i "$iface" -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-server"
_tcp_out_accept -o "$iface" -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-server"
fi
else
#local cidr=${nic_}CIDR
#local cidr=${nemonic}_CIDR
local cidr=${nemo[cidr]}
if $udp; then
_udp_in_accept -i "$iface" -s $cidr -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-server"
_udp_out_accept -o "$iface" -d $cidr -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-server"
fi
if $tcp; then
_tcp_in_accept -i "$iface" -s $cidr -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-server"
_tcp_out_accept -o "$iface" -d $cidr -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-server"
fi
fi
prev_IFACE=$iface
done
;;

FNICSA)
#echo sfsfdsdf
#DEBUG=3
_debug3 "FNICSA[@]: ${FNICSA[@]}"

prev_IFACE=

for nemonic in ${FNICSA[@]}; do  # nemonic=MNIC|XNIC...

# remove leading whitespace characters
nemonic="${nemonic#"${nemonic%%[![:space:]]*}"}"
# remove trailing whitespace characters
nemonic="${nemonic%"${nemonic##*[![:space:]]}"}" 

[[ -v NEMOS[$nemonic] ]] || {

_warn "Missing NEMOS[$nemonic]"
continue
}

declare -n nemo=${NEMOS[$nemonic]}

[[ -v nemo[iface] ]] || {

_warn "Missing nemo[iface]"
continue
}

local iface=${nemo[iface]}

_debug3 "nemonic: $nemonic"
#nic_IFACE=${nemonic}_IFACE
_debug3 "nic_IFACE: $nic_IFACE, prev_IFACE: $prev_IFACE"
#[[ -n "$prev_IFACE" && "$iface" == "$prev_IFACE" ]] && continue
[[ "$iface" == "$prev_IFACE" ]] && continue

_debug3
if [[ "$nemonic" == 'XNIC' ]]; then
if $udp; then
_udp_in_accept -i "$iface" -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-server"
_udp_out_accept -o "$iface" -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-server"
fi
if $tcp; then
_tcp_in_accept -i "$iface" -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-server"
_tcp_out_accept -o "$iface" -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-server"
fi
else
#local cidr=${nic_}CIDR
#local cidr=${nemonic}_CIDR
local cidr=${nemo[cidr]}
_debug_network "network: $network, prefix: $prefix"
#_debug_network "!network: ${!network}, !prefix: ${!prefix}"  # network: invalid indirect expansion
_debug_network "$(env | grep MNIC)"
_debug_network "udp: $udp"
if $udp; then
#_udp_in_accept -i "$iface" -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-server"
#_udp_out_accept -o "$iface" -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-server"
_udp_in_accept -i "$iface" -s $cidr -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-server"
_udp_out_accept -o "$iface" -d $cidr -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-server"
fi
if $tcp; then
_debug_network "_tcp_in_accept -i \"$iface\" -s $cidr -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment \"${comment}-server\""
_tcp_in_accept -i "$iface" -s $cidr -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-server"
_tcp_out_accept -o "$iface" -d $cidr -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-server"
#_tcp_in_accept -i "$iface" -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-server"
#_tcp_out_accept -o "$iface" -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-server"
fi
fi
prev_IFACE=$iface
done
:<<\_c
default: iptables -A PICASSO_INPUT -p tcp -j ACCEPT -i enp0s8 -s 192.168.1.0/24 -m multiport --dports 80,443 -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment -server
default: iptables -A PICASSO_OUTPUT -p tcp -j ACCEPT -o enp0s8 -d 192.168.1.0/24 -m multiport --sports 80,443 -m conntrack --ctstate ESTABLISHED -m comment --comment -server
default: iptables -A PICASSO_INPUT -p tcp -j ACCEPT -i enp0s9 -m multiport --dports 80,443 -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment -server
default: iptables -A PICASSO_OUTPUT -p tcp -j ACCEPT -o enp0s9 -m multiport --sports 80,443 -m conntrack --ctstate ESTABLISHED -m comment --comment -server
_c
;;

*)

echo "why am i here - fix this"

_debug3 "*)"

#if [[ -n "$MNIC_IP" && "$nemonic" == "$MNIC_IP" ]]; then
if [[ "$nemonic" == "$MNIC_IP" ]]; then
if $udp; then
_udp_in_accept -i "$MNIC_IFACE" -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-server"
_udp_out_accept -o "$MNIC_IFACE" -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-server"
fi
if $tcp; then
#_tcp_in_accept -i "$MNIC_IFACE" -s $MNIC_CIDR -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-server"
#_tcp_out_accept -o "$MNIC_IFACE" -d $MNIC_CIDR -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-server"
_tcp_in_accept -i "$MNIC_IFACE" -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-server"
_tcp_out_accept -o "$MNIC_IFACE" -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-server"
fi
#elif [[ -n "$XNIC_IP" && "$nemonic" == "$XNIC_IP" ]]; then
elif [[ "$nemonic" == "$XNIC_IP" ]]; then
if $udp; then
_udp_in_accept -i "$XNIC_IFACE" -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-server"
_udp_out_accept -o "$XNIC_IFACE" -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-server"
fi
if $tcp; then
#_tcp_in_accept -i "$XNIC_IFACE" -s $XNIC_CIDR -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-server"
#_tcp_out_accept -o "$XNIC_IFACE" -d $XNIC_CIDR -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-server"
_tcp_in_accept -i "$XNIC_IFACE" -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-server"
_tcp_out_accept -o "$XNIC_IFACE" -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-server"
fi
else
_debug3 "catch-all nemonic: $nemonic, ports: $ports, comment: $comment"
if $udp; then
# $nemonic is an ip
_udp_in_accept -i $iface -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-server"
_udp_out_accept -o $iface -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-server"
fi
if $tcp; then
# $interface is an ip
_tcp_in_accept -i $iface -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-server"
_tcp_out_accept -o $iface -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-server"
fi
fi
;;
esac
done

return 0
}
export -f _open_server_firewall


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
:<<\__c
_open_client_firewall <interface> <remote destination port/s, remote source port/s> <comment>

_open_client_firewall MNIC 22 'ssh'
_open_client_firewall XNIC "80,443" 'www'

clients typically connect from random ports above 1024

local       remote
any port -> 80
any port -> 445

this assumes _iptables-input-drop-forward-drop-output-accept, because dports always refers to the remote port and does not impede any local port destinations
__c
:<<\__c
TODO:
for now i avoid the random port stuff by omitting the local source port

we could extend things like so:
_open_client_firewall FNICSA any:445 'Samba'
_open_client_firewall FNICSA any:80 'Samba'
__c


function _open_client_firewall() {
_debug3 "_open_client_firewall $@"

case $1 in
MNIC_IP) local interfaces=MNIC;;
XNIC_IP) local interfaces=XNIC;;
*) local interfaces=$1;;
esac

local ports=$2
local comment=$3

if [[ -n "$4" ]]; then
tcp=false
echo $4 | grep -q -i 'tcp' && tcp=true
udp=false
echo $4 | grep -q -i 'udp' && udp=true
else
tcp=true  # default is tcp
udp=false
fi
_debug3 "wfow7fs90fwjf2 interfaces: $interfaces, udp: $udp, tcp: $tcp, ports: $ports, comment: $comment"

local nemonic

for nemonic in ${interfaces//,/ }; do

#_debug "ssfd nemonic: $nemonic"

[[ -v NEMOS[$nemonic] ]] || {

_warn "Missing NEMOS[$nemonic]"
continue
}

declare -n nemo=${NEMOS[$nemonic]}

[[ -v nemo[iface] ]] || {

_warn "Missing nemo[iface]"
continue
}

local iface=${nemo[iface]}
local cidr=${nemo[cidr]}

_debug_firewall "$nemonic - iface: $iface, cidr: $cidr"

case $nemonic in

MNIC)

if $tcp; then
#_debug3 "dfdljljfd iface: "$iface", cidr: $cidr, ports: $ports, comment: $comment"
_tcp_out_accept -o "$iface" -d $cidr -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-client"
_tcp_in_accept -i "$iface" -s $cidr -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-client"
#_tcp_out_accept -o "$iface" -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-client"
#_tcp_in_accept -i "$iface" -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-client"
fi
if $udp; then
_udp_out_accept -o "$iface" -d $cidr -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-client"
_udp_in_accept -i "$iface" -s $cidr -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-client"
#_udp_out_accept -o "$iface" -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-client"
#_udp_in_accept -i "$iface" -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-client"
fi
;;

XNIC)

if $tcp; then
_tcp_out_accept -o "$iface" -d 0.0.0.0/0 -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-client"
_tcp_in_accept -i "$iface" -s 0.0.0.0/0 -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-client"
#_tcp_out_accept -o "$iface" -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-client"
#_tcp_in_accept -i "$iface" -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-client"
fi
if $udp; then
_udp_out_accept -o "$iface" -d 0.0.0.0/0 -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-client"
_udp_in_accept -i "$iface" -s 0.0.0.0/0 -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-client"
#_udp_out_accept -o "$iface" -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-client"
#_udp_in_accept -i "$iface" -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-client"
fi
;;

SNIC|TNIC)

if $tcp; then
_tcp_out_accept -o "$iface" -d $cidr -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-client"
_tcp_in_accept -i "$iface" -s $cidr -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-client"
fi
if $udp; then
_udp_out_accept -o "$iface" -d $cidr -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-client"
_udp_in_accept -i "$iface" -s $cidr -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-client"
fi
;;

NICSA)

for nemonic in "${NICSA[@]}"; do

# remove leading whitespace characters
nemonic="${nemonic#"${nemonic%%[![:space:]]*}"}"
# remove trailing whitespace characters
nemonic="${nemonic%"${nemonic##*[![:space:]]}"}" 

_debug3 "s9s6flf0wf nemonic: $nemonic"

[[ -v NEMOS[$nemonic] ]] || {

_warn "Missing NEMOS[$nemonic]"

continue
}

declare -n nemo=${NEMOS[$nemonic]}

[[ -v nemo[iface] ]] || {

_warn "Missing nemo[iface]"

continue
}

local iface=${nemo[iface]}

_debug3 "s9s6flf0wf iface: $iface"

#nic_IFACE=${nemonic}_IFACE
if [[ "$nemonic" == 'XNIC' ]]; then
if $tcp; then
#_tcp_out_accept -o "$iface" -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-client"
#_tcp_in_accept -i "$iface" -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-client"
_tcp_out_accept -o "$iface" -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-client"
_tcp_in_accept -i "$iface" -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-client"
fi
if $udp; then
#_udp_out_accept -o "$iface" -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-client"
#_udp_in_accept -i "$iface" -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-client"
_udp_out_accept -o "$iface" -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-client"
_udp_in_accept -i "$iface" -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-client"
fi
else
#local cidr=${nic_}CIDR
#local cidr=${nemonic}_CIDR

if $tcp; then
#_tcp_out_accept -o "$iface" -d ${!cidr} -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-client"
#_tcp_in_accept -i "$iface" -s ${!cidr} -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-client"
_tcp_out_accept -o "$iface" -d $cidr -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-client"
_tcp_in_accept -i "$iface" -s $cidr -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-client"
fi
if $udp; then
#_udp_out_accept -o "$iface" -d ${!cidr} -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-client"
#_udp_in_accept -i "$iface" -s ${!cidr} -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-client"
_udp_out_accept -o "$iface" -d $cidr -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-client"
_udp_in_accept -i "$iface" -s $cidr -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-client"
fi
fi
done
;;

FNICSA)

for nemonic in ${FNICSA[@]}; do

# remove leading whitespace characters
nemonic="${nemonic#"${nemonic%%[![:space:]]*}"}"
# remove trailing whitespace characters
nemonic="${nemonic%"${nemonic##*[![:space:]]}"}" 

[[ -v NEMOS[$nemonic] ]] || {

_warn "Missing NEMOS[$nemonic]"

continue
}

declare -n nemo=${NEMOS[$nemonic]}

[[ -v nemo[iface] ]] || {

_warn "Missing nemo[iface]"

continue
}

local iface=${nemo[iface]}

_debug3 "slsfsfwwlfsofu nemonic: $nemonic"
#nic_IFACE=${nemonic}_IFACE
if [[ "$nemonic" == 'XNIC' ]]; then
if $tcp; then
_tcp_out_accept -o "$iface" -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-client"
_tcp_in_accept -i "$iface" -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-client"
fi
if $udp; then
_udp_out_accept -o "$iface" -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-client"
_udp_in_accept -i "$iface" -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-client"
fi
else
#local cidr=${nic_}CIDR
#local cidr=${nemonic}_CIDR
if $tcp; then
_tcp_out_accept -o "$iface" -d $cidr -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-client"
_tcp_in_accept -i "$iface" -s $cidr -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-client"
fi
if $udp; then
_udp_out_accept -o "$iface" -d $cidr -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-client"
_udp_in_accept -i "$iface" -s $cidr -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-client"
fi
fi
done
;;

*)

echo "why am i here - fix this"

_debug_firewall "ada nemonic: $nemonic, NEMO_MNIC[iface]: ${NEMO_MNIC[iface]}, NEMO_XNIC[iface]: ${NEMO_XNIC[iface]}"

:<<\_j
#if [[ -n "$MNIC_IP" && "$interface" == "$MNIC_IP" ]]; then
if [[ -v NEMOS[MNIC] && "$interface" == "${NEMO_MNIC[iface]}" ]]; then

if $tcp; then
#_tcp_out_accept -o "$MNIC_IFACE" -d $MNIC_CIDR -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-client"
#_tcp_in_accept -i "$MNIC_IFACE" -s $MNIC_CIDR -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-client"
#_tcp_out_accept -o "$MNIC_IFACE" -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-client"
#_tcp_in_accept -i "$MNIC_IFACE" -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-client"
_tcp_out_accept -o "$interface" -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-client"
_tcp_in_accept -i "$interface" -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-client"
fi
if $udp; then
#_udp_out_accept -o "$MNIC_IFACE" -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-client"
#_udp_in_accept -i "$MNIC_IFACE" -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-client"
_udp_out_accept -o "$interface" -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-client"
_udp_in_accept -i "$interface" -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-client"
fi

#elif [[ -n "$XNIC_IP" && "$interface" == "$XNIC_IP" ]]; then
elif [[ -v NEMOS[XNIC] && "$interface" == "${NEMO_XNIC[iface]}" ]]; then

if $tcp; then
#_tcp_out_accept -o "$XNIC_IFACE" -d $XNIC_CIDR -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-client"
#_tcp_in_accept -i "$XNIC_IFACE" -s $XNIC_CIDR -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-client"
#_tcp_out_accept -o "$XNIC_IFACE" -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-client"
#_tcp_in_accept -i "$XNIC_IFACE" -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-client"
_tcp_out_accept -o "$interface" -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-client"
_tcp_in_accept -i "$interface" -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-client"
fi
if $udp; then
#_udp_out_accept -o "$XNIC_IFACE" -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-client"
#_udp_in_accept -i "$XNIC_IFACE" -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-client"
_udp_out_accept -o "$interface" -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-client"
_udp_in_accept -i "$interface" -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-client"
fi

else
_j
_debug3 "catch-all interface: $interface, ports: $ports, comment: $comment"
if $tcp; then
# $interface is an ip
_tcp_out_accept -o $iface -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-client"
_tcp_in_accept -i $iface -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-client"
fi
if $udp; then
# $interface is an ip
_udp_out_accept -o $iface -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-client"
_udp_in_accept -i $iface -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-client"
fi

:<<\_j
fi
_j
;;
esac

done

return 0
}
export -f _open_client_firewall


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
:<<\_c
. iptables.fun && _iptables_accept_all
_c

function _iptables_accept_all() {

sudo iptables -F
sudo iptables -X
sudo iptables -t nat -F
sudo iptables -t nat -X
sudo iptables -t mangle -F
sudo iptables -t mangle -X
sudo iptables -P INPUT ACCEPT
sudo iptables -P FORWARD ACCEPT
sudo iptables -P OUTPUT ACCEPT

sudo iptables -N TEMP_IN  # necessary for extraneous script that may be expecting this namespace to exist
sudo iptables -N TEMP_OUT  # necessary for extraneous script that may be expecting this namespace to exist
}


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
:<<\_x
_open_server_firewall MNIC 80,443 test
_open_client_firewall MNIC 80,443 test
sudo iptables -L INPUT
sudo iptables -L OUTPUT
sudo iptables -L PICASSO_INPUT
sudo iptables -L PICASSO_OUTPUT

sudo journalctl -f | grep IPT
_x


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
true
