:<<\_c
longopts - consumes all variables preceded with '--'
_c

:<<\_x
_test() {
. $PROOT/$PV/basebox/bin/longopts.sh

declare -p longopts
declare -p longvals

for i in "${!longopts[@]}"; do
opt=${longopts[$i]}
val=${longvals[$i]}
echo "opt: $opt, val: $val"
done

echo "@: $@"
}; _test --test="1 2 3 4 5" --test2="6 7 8 9 0" test3
_x

#echo "longopts.sh @: $@"

# marshal commandline arguments/values into the bash array: longopts
declare -a longopts=()
declare -a longvals=()

optspec=":v-:"  # ':' - leading colon turns on silent error reporting, 'v' first so DEBUG is set early, trailing ':' means to expect a parameter

other=()

# options are contained in $@

#echo "longopts IFS: $IFS, @: $@"

#echo slsdfslsdfldsf
#for OPTARG in "$@"; do
for OPTARG in $@; do

#echo "longopts OPTARG: $OPTARG"
  case "${OPTARG}" in
    --) break;;

    --*)
#echo "OPTARG:  ${OPTARG[*]}"
#echo "OPTIND:  ${OPTIND[*]}"

      OPTARG="${OPTARG:2}"  # remove '--'

# is there a value assigned to it '='
if [[ "$OPTARG" =~ '=' ]]; then

      val=${OPTARG#*=}  # split on '='
      opt=${OPTARG%=$val}
#echo "opt: $opt, val: $val"

      if [[ "$val" == "$OPTARG" ]]; then

        # no equal sign found

        _error "Undefined argument: '-${OPTARG}'"
        echo "do you mean: --${OPTARG}=<value>"
        break
      fi

#echo "--${OPTARG}" " " "${val}"

#echo "$opt: longvals+=($val)"

      longopts+=("$opt")
      longvals+=("$val")
else
      longopts+=($OPTARG)
      longvals+=("")
fi
      ;;

    *)
#echo "other+=("$OPTARG")"

other+=("$OPTARG")
      ;;

    esac

#shift
done

#echo "longopts.sh - set -- \"${other[@]}\""

set -- "${other[@]}"  # restore other parameters


# ----------
:<<\_x
function try() {

#OPTARGS="--name=foo bar" . $PR/$PV/basebox/bin/longopts.sh  # <- $@

echo "opts: ${longopts[@]}"
echo "vals: ${longvals[@]}"
echo "other: $@"
}

try --name=foo bar
_x
