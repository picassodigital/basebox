[[ -v DEBUG ]] && (( DEBUG >= 0 )) && printf "\e[1;38;5;228m>>> %s @: %s\e[0m\n" "${BASH_SOURCE[0]}" "${*@Q}"

:<<\_c
. guest-environment.sh

see: $PROOF/picasso-init/picasso-init.md

guests load their environment from two sources...
1) _sourcedir $PR/$PV/bin/ - core environment (basebox.tar/instance.tar)
2) _sourcedir $PR/bin/ - system provisioning environment
3) _sourcedir $PICASSO_HOME_DOTDIR/bin/ - user provisioning environment
_c

:<<\_x
. guest-environment.sh

or,

[[ -v _GENV_ ]] || . guest-environment.sh
_x

#echo "guest-bash-env000 _GENV_: $_GENV_"
#DEBUG=2
#PDEBUG=2


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
# source guest environment

#[[ -v _GENV_ ]] && echo "WARN: rentry of _GENV_: $_GENV_"

_GENV_=$((_GENV_++))  # we don't export this value. in bash, subshells do not inherit aliases which we may have defined in init.d. to include those aliases in our environment we must reload our environment in subshells.

#echo "guest-environment.sh _GENV_=$_GENV_"

PICASSO_HOME_DOTDIR=${PICASSO_HOME_DOTDIR:-~/.picasso}

# ----------
function _sourcedir() {

[[ -d "$1" ]] && {

#_debug3 "$(ls -l $1)"

for script in $(/usr/bin/find $1 -maxdepth 1 -name '[0-9]*.*' \( -type l -o -type f \) | /usr/bin/sort); do

#_debug3 "_f_sourcedir1 script: $script"

#. $script || { echo ". $script"; sleep 10; exit 1; }
. $script || { echo ". $script"; sleep 10; return 1; }

#_debug3 "_f_sourcedir2 script: $script"

done

true
}

}


# ----------
#echo "vvs _sourcedir $PR/$PV/bin/"  # no _debug() yet

_sourcedir $PR/$PV/bin/ || return 1  # ro - first, load disto functionality -> _debug(), $PICASSO_HOME_DOTDIR

_debug3 "wfee _sourcedir $PR/bin/"

_sourcedir $PR/bin/ || return 1  # rw - second, load system configuration

[[ -d $PICASSO_HOME_DOTDIR/bin ]] && {

_debug3 "sdf _sourcedir $PICASSO_HOME_DOTDIR/bin/"

_sourcedir $PICASSO_HOME_DOTDIR/bin/ || return 1  # third, load user configuration
}

_debug3 "guest-environment.sh done"

true
