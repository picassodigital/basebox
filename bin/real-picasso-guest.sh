[[ -v DEBUG ]] && (( DEBUG >= 0 )) && printf "\e[1;38;5;228m>>> %s @: %s\e[0m\n" "${BASH_SOURCE[0]}" "${*@Q}"

:<<\_c
. real-picasso-guest.sh

NB: this file is part of 'basebox.git' which gets embedded within each basebox - iow: changes to this file require a rebuild of the basebox

this script receives arguments that a provider may pass to it
ie: Vagrant passes through arguments that originate within the Vagrantfile
in addition, Vagrant also sets the environment variable PROVISIONER=<name of current provisioning script file>
note that arguments are received as pairs (--<name>=[value])

see: $NOTES/variables/PROVISIONER_ENV.md
proof: $DESKSP/proof/PROVISIONER_USER

[assumptions]
provisioner-env) file-system based endpoints require vboxsf

[usage]
. picasso-guest.sh $@  # aka real-picasso-guest.sh
...
_c

:<<\_c
we load _debug functions just for support between here and the bottom of the file when we run '. guest-environment.sh'
this would not be necessary if there were no debugging lines between here and '. guest-environment.sh'
_c

if (( DEBUG >= 1 )); then
. 000-guest-debug.sh
else
# noops
function _debug() { :; }
function _debug2() { :; }
function _debug3() { :; }
function _debug_network() { :; }
fi

#echo "real-picasso-guest.sh 0: $0, @: $@"
#DEBUG=2

#echo "ghsl USER: $USER"

# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
:<<\_c
only the first provisioner processes this...
chained provisioners never get here
_c

ARG0=$0

. longopts.sh $@  # <- $@(--*=*) -> longopts[], longvals[], $@

#echo "longopts[]: ${longopts[@]}"

other=()

PROVISIONER_NAME=unknown
PROVISIONER_USER=${PROVISIONER_USER:-picasso}  # hard-coded default in case it's not declared

for i in ${!longopts[@]}; do
opt=${longopts[$i]}
val=${longvals[$i]}

#echo "sfsdf opt: $opt, val: $val"

case $opt in

provisioner-name) PROVISIONER_NAME=$val;;

provisioner-user) PROVISIONER_USER=$val;;

provisioner-env) : ;;

*)
if [[ -n "$val" ]]; then
other+=("--${opt}=$val")
else
other+=("--${opt}")
fi
;;

esac

done

#echo "sdfds other[@]: ${other[@]}, @: $@"

set -- "${other[@]}" # restore other parameters

#echo "sdfds2 other[@]: ${other[@]}, @: $@"


# ----------
:<<\_c
USER=root | USER=$PROVISIONER_USER
_c

[[ "$PROVISIONER_USER" != "$USER" ]] && {

sudo -Hin -u $PROVISIONER_USER "$ARG0" $@; exit $?  # re-enter!
}


:<<\_c
USER=$PROVISIONER_USER
_c

#echo "USER: $USER"

PICASSO_HOME=/home/$USER
PICASSO_HOME_DOTDIR=$PICASSO_HOME/.picasso

PROVISIONER_ENV=$PICASSO_HOME_DOTDIR/bin/020-provisioner.env


# ----------
#echo ". guest-environment.sh $@"

. guest-environment.sh $@ || exit $?  # <- $PR/bin/provider.env, $PICASSO_HOME_DOTDIR/bin/020-provisioner.env -> env, _debug...

[[ -n "$PROVISIONER_NAME" ]] && _info "Running provisioner '$PROVISIONER_NAME' as '$USER'"


# ----------
true
