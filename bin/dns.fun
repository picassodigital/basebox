:<<\_c
[usage]
. $PR/$PV/basebox/bin/dns.fun

_dns_set test.picasso.digital. 1.2.3.4

FDOM=bit.cafe; _dns_set test 1.2.3.4
FDOM=bit.cafe; _dns_get test.$FDOM
nslookup test.$FDOM

PNAME=kv
_dns_set ${PNAME}.bit.cafe. 10.0.0.12

FDOM=bit.cafe _dns_set $PNAME 10.0.0.12
FDOM=picasso.digital _dns_get identity

FDOM=bit.cafe {FDOM}_DNS_KEY_FILE=$WORKSP_NAMESP_DOTDIR/${FDOM}.key _dns_set $PNAME 10.0.0.12

_dns_set $PNAME $ip
FDOM=bit.cafe {FDOM}_DNS_KEY_FILE=$WORKSP_NAMESP_DOTDIR/${FDOM}.key _dns_set $PNAME $ip
FDOM=bit.cafe {FDOM}_DNS_KEY_FILE=$WORKSP_NAMESP_DOTDIR/${FDOM}.key _dns_set test 192.168.1.254

_dns_get ${PNAME}.${FDOM}
_c

_debug "DESKSP/$PV/basebox/bin/dns.fun DNS_ENDPOINT: $DNS_ENDPOINT"


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
function _dns_get() {

[[ -z "$@" ]] && { echo "<FQDN>"; return 1; }

:<<\_c
unset or empty DNS_ENDPOINT implies that the dns should be handled dynamically by fetching its value from KV each time it is needed
_c
[[ -z "$DNS_ENDPOINT" ]] && {

_debug_dns "DNS_ENDPOINT=\$(_kv_get DNS_ENDPOINT)"

local DNS_ENDPOINT=$(_kv_get DNS_ENDPOINT) || { _error "_kv_get DNS_ENDPOINT"; return 1; }  # <- $FDOM
}

_debug_dns "DNS_ENDPOINT: $DNS_ENDPOINT"

_f_url_parse $DNS_ENDPOINT DNS || { _error "_f_url_parse $DNS_ENDPOINT DNS"; return 1; }  # -> DNS[*]


local fqdn_dot=$1

#[[ "$fqdn_dot" != *. ]] && {  # does it end with a period (test.picasso.digital.)?
[[ "$fqdn_dot" == *.* ]] || {  # does it include a period (test.picasso.digital.)?

# it does not include a period

[[ -n "$FDOM" ]] && fqdn_dot+=".${FDOM}."  # append custom search
}

local r="$(nslookup $fqdn_dot ${DNS[host]})"

[[ $? -eq 0 ]] || { _error "DNS entry not found for $1"; return 1; }

r=$(echo "$r" | awk -F':' '/^Address: / {matched = 1} matched {print $2}')
[[ -n "$r" ]] || return 1

echo $r  # return $r
}
export -f _dns_get


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
# copied from host/bin/dns.fun...

function _dns_set() {  # <- $FDOM
local FDOM=$FDOM
local fqdn_dot=$1

_debug_dns "_dns_set @: $@, FDOM: $FDOM"

[[ "${DNS_DISABLE^^}" == 'TRUE' ]] && {

_warn "_dns_set - DNS_DISABLE: $DNS_DISABLE"
return 0
}

[[ -z "$1" || -z "$2" ]] && {
echo "_dns_set $@"
echo "[usage}: _dns_set <FQDN> <IP>"
return 1
}

if [[ "$fqdn_dot" == *.* ]]; then  # does it include a period?

# yes, it does include a period

# fqdn_dot=test.picasso.digital.

if [[ "$fqdn_dot" == *. ]]; then  # does it end with a period?

# yes, it ends with a period

:<<\_j
#key=${fqdn_dot::-1}  # strip trailing '.'
#key=${key#*.}_DNS_KEY  # strip hostname
#key=${key//./_}  # runnable.org -> runnable_org
_j
FDOM=${fqdn_dot::-1}  # strip trailing '.' - FDOM=test.picasso.digital
FDOM=${FDOM#*.}  # strip hostname - FDOM=picasso.digital

else

:<<\_j
#key=DNS_KEY
#key=${fqdn_dot#*.}_DNS_KEY  # strip hostname
#key=${key//./_}  # runnable.org -> runnable_org
_j
FDOM=${fqdn_dot#*.}  # strip hostname - FDOM=picasso.digital

fqdn_dot+="."  # fqdn_dot=test.picasso.digital.

fi

else

# no, it does not include a period - 

[[ -n "$FDOM" ]] && fqdn_dot+=".${FDOM}."  # append custom search

fi

:<<\_c
unset or empty DNS_ENDPOINT implies that the dns should be handled dynamically by fetching its value from KV each time it is needed
_c
[[ -z "$DNS_ENDPOINT" ]] && {

_debug_dns "DNS_ENDPOINT=\$(_kv_get DNS_ENDPOINT)"

local DNS_ENDPOINT=$(_kv_get DNS_ENDPOINT) || { _error "_kv_get DNS_ENDPOINT"; return 1; }  # <- $FDOM
}

_debug_dns "DNS_ENDPOINT: $DNS_ENDPOINT"

_f_url_parse $DNS_ENDPOINT DNS || { _error "_f_url_parse $DNS_ENDPOINT DNS"; return 1; }  # -> DNS[*]

:<<\_x
fqdn=www.picasso.digital
ip=1.2.3.4
_x

_debug_dns "s=\$(FDOM=$FDOM _kv_get DNS_KEY)"

local s=$(FDOM=$FDOM _kv_get DNS_KEY); [[ -z "$s" ]] && { _error "KV not found ${FDOM}DNS_KEY)"; return 1; }  # <- $FDOM
:<<\_x
adomain=runnable.org
FDOM=$adomain _kv_get DNS_KEY
FDOM= _kv_get ${adomain//./_}DNS_KEY
_x


(( DEBUG_DNS > 0 )) && {
echo "cat <<! | nsupdate -k <(echo $s)
server ${DNS[host]}
update delete $fqdn_dot A
update add $fqdn_dot 3600 A $2
send
!
"
}

cat <<! | nsupdate -k <(echo $s)
server ${DNS[host]}
update delete $fqdn_dot A
update add $fqdn_dot 3600 A $2
send
!

}
#export -f _dns_set


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
function _dns_delete() {

[[ -z "$1" ]] && {
echo "_dns_delete $@"
echo "[usage}: _dns_delete <FQDN>"
return 1
}

[[ -z "$DNS_HOST" ]] && _warn "DNS_HOST is not set"

local key_name

local fqdn_dot=$1
local key s

if [[ "$fqdn_dot" == *.* ]]; then  # does it include a period?

# fqdn_dot=test.picasso.digital

if [[ "$fqdn_dot" == *. ]]; then  # does it end with a period?

# fqdn_dot=test.picasso.digital.

key=${fqdn_dot::-1}  # strip trailing '.'

# key=test.picasso.digital

key=${key#*.}DNS_KEY

else

key=${fqdn_dot#*.}DNS_KEY

fqdn_dot+="."  # fqdn_dot=test.picasso.digital.

fi

else

# it does not include a period

[[ -n "$FDOM" ]] && fqdn_dot+=".${FDOM}."  # append custom search

key=${FDOM}DNS_KEY

fi

s=$(_kv_get $key); [[ -z "$s" ]] && _error_return "$key not found"

cat <<! | nsupdate -k <(echo $s)
server $DNS_HOST
update delete $fqdn_dot A
send
!

}
#export -f _dns_delete
