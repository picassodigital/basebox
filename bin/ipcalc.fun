#!/usr/bin/env bash

:<<\_c
_ipcalc_out <cidr> [prefix]
_c

:<<\_x
. ipcalc.fun 2.2.3.4/24
echo $IP
echo $CIDR
echo $NETWORK
echo $PREFIX
echo $NETMASK
echo $BROADCAST

. ipcalc.fun

_ipcalc_out $XNIC_CIDR XNIC_IFACE
_x

:<<\_x
XNIC_CIDR=10.0.0.1/24
. ipcalc.fun $XNIC_CIDR XNIC_IFACE
_x

:<<\_x
test_CIDR=10.0.0.1/24
#declare $(ipcalc.fun $test_CIDR test_)
. ipcalc.fun $test_CIDR test_
echo $test_IP
echo $test_CIDR
echo $test_NETWORK
echo $test_PREFIX
echo $test_NETMASK
echo $test_BROADCAST
_x

:<<\_x
#declare $(ipcalc.fun 10.0.0.1/24)
. ipcalc.fun 10.0.0.1/24
echo $IP
echo $CIDR
echo $NETWORK
echo $PREFIX
echo $NETMASK
echo $BROADCAST
_x


# ----------
function _ipcalc() {

#_debug_network "_ipcalc @: $@"

local PED="$( cd "$(dirname "${BASH_SOURCE[0]}")" && pwd )"

local a n m b

local ipcalc_response="$($PED/ipcalc describe $1)"
:<<\_x
ipcalc_response="$(ipcalc describe 10.0.0.1/24)"
_x

#_debug_network "$(echo "$ipcalc_response")"

a=$(echo "$ipcalc_response" | grep "^Address:" | awk '{print $2}')
declare -g ${2}IP=${a#*=}

#_debug_network "declare -g ${2}IP=${a#*=}"

n=$(echo "$ipcalc_response" | grep "^Network:" | awk '{print $2}')
declare -g ${2}NETWORK=${n#*=}
declare -g ${2}NETWORK_C=${n%.*}
declare -g ${2}PREFIX=${n#*/}

#_debug_network "declare -g ${2}NETWORK=${n#*=}"
#_debug_network "declare -g ${2}NETWORK_C=${n%.*}"

m=$(echo "$ipcalc_response" | grep "^Netmask:" | awk '{print $2}')
declare -g ${2}NETMASK=${m#*=}

b=$(echo "$ipcalc_response" | grep "^Broadcast:" | awk '{print $2}')
declare -g ${2}BROADCAST=${b#*=}

declare -g ${2}CIDR=${n#*=}

true
}


# ----------
:<<\_j
function _ipcalc_out() {

local PED="$( cd "$(dirname "${BASH_SOURCE[0]}")" && pwd )"

local a n m b

local ipcalc_response="$($PED/ipcalc describe $1)"
:<<\_x
ipcalc_response="$($PR/$PV/basebox/bin/ipcalc describe 10.0.0.1/24)"
_x

a=$(echo "$ipcalc_response" | grep "^Address:" | awk '{print $2}')
declare -g ${2}IP=${a#*=}

_debug_network "declare -g ${2}IP=${a#*=}"

n=$(echo "$ipcalc_response" | grep "^Network:" | awk '{print $2}')
declare -g ${2}NETWORK=${n#*=}
declare -g ${2}NETWORK_C=${n%.*}

_debug_network "declare -g ${2}NETWORK=${n#*=}"
_debug_network "declare -g ${2}NETWORK_C=${n%.*}"

m=$(echo "$ipcalc_response" | grep "^Netmask:" | awk '{print $2}')
declare -g ${2}NETMASK=${m#*=}

b=$(echo "$ipcalc_response" | grep "^Broadcast:" | awk '{print $2}')
declare -g ${2}BROADCAST=${b#*=}

declare -g ${2}CIDR=${n#*=}

true
}
_j

# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
[[ -n "$@" ]] && {

:<<\_j
#echo "$(_ipcalc_out $@)"
#_ipcalc_out $@
_j

_ipcalc $@
}
