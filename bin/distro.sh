[[ -v DEBUG ]] && (( DEBUG >= 0 )) && printf "\e[1;38;5;228m>>> %s @: %s\e[0m\n" "${BASH_SOURCE[0]}" "${*@Q}"

# this is a bootstrapping network file only and need not reside on the target machine

:<<\_c

[usage]

# direct all variables to the environment

. $PR/$PV/basebox/bin/distro.sh

# direct all variables to a file that can subsequently be loaded in the environment

$PR/$PV/basebox/bin/distro.sh $PR/bin/000-distro.env && . $PR/bin/000-distro.env


[test]

$PR/$PV/basebox/bin/distro.sh /dev/tty

$PR/$PV/basebox/bin/distro.sh $PR/bin/000-distro.env && cat $PR/bin/000-distro.env
_c


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
shopt -s expand_aliases  # aliases are not expanded when the shell is not interactive, unless the expand_aliases shell option is set using shopt

if [[ -n "$1" ]]; then
output=$1
#> $output  # purge
alias __output='cat >> $output <<__output'
alias __output_strong='cat >> $output <<\__output_strong'
else
alias __output=''  # __
alias __output_strong=''  # __
fi

__output

# ${BASH_SOURCE[0]}...
__output


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
arch=$(uname -m)
kernel=$(uname -r)

if [[ "$(expr substr $(echo $(uname -s) | tr -s '[:upper:]' '[:lower:]') 1 5)" == 'linux' ]]; then

# yes this is linux...

#if [[ "$kernel" =~ Microsoft ]]; then

if [[ -f "/etc/lsb-release" ]]; then

os=$(lsb_release -s -d)
export OS=ubuntu
#export OSV=$(lsb_release -sr)

:<<\_s
# setting this is global and peristent - the drawback is that manual installs are also affected which is unexpected by the user
echo 'debconf debconf/frontend select Noninteractive' | sudo debconf-set-selections
_s

elif [[ -f "/etc/debian_version" ]]; then

os="debian $(</etc/debian_version)"
export OS=debian

:<<\_s
echo 'debconf debconf/frontend select Noninteractive' | sudo debconf-set-selections
_s

elif [[ -f "/etc/fedora-release" ]]; then

export OS=fedora

. /etc/fedora-release
#export OSV=$VERSION_ID

export releasever=$(rpm -q --qf "%{version}\n" --whatprovides redhat-release)
export basearch=$(uname -m)

elif [[ -f "/etc/redhat-release" ]]; then

os=`cat /etc/redhat-release`
export OS=redhat

export releasever=$(rpm -q --qf "%{version}\n" --whatprovides redhat-release)
export basearch=$(uname -m)

elif [[ -f "/etc/arch-release" ]]; then

export OS=arch

else

export OS="$(echo $(uname -s) | tr -s '[:upper:]' '[:lower:]') $(uname -r)"

fi

export SYSTEMD_VERSION=$(systemctl --version 2> /dev/null | head -n1 | awk '{print $2}')

:<<\_s
elif [[ "$(echo $(uname -o) | tr -s '[:upper:]' '[:lower:]')" == "cygwin" ]]; then

#export OS=cygwin
export OS=Windows_NT

elif [[ "$(echo $(uname -o) | tr -s '[:upper:]' '[:lower:]')" == "msys" ]]; then

#export OS=msys
export OS=Windows_NT
_s

fi


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
#get_distro

__output
export OS=$OS
__output


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
:<<\_c
https://peteris.rocks/blog/quiet-and-unattended-installation-with-apt-get/
_c

case $OS in

arch)

__output_strong

function _is_installed() { echo TODO; }

function _install() { 
if (( DEBUG == 0 )); then
sudo 1>/dev/null pacman --noconfirm -S "$@"
elif (( DEBUG < -1 )); then
sudo &>/dev/null pacman --noconfirm -S "$@"
elif (( DEBUG < -0 )); then
sudo 1>/dev/null 2>&1 pacman --noconfirm -S "$@"
else
pacman --noconfirm -S "$@"
fi
}
export -f _install
__output_strong
;;


fedora)

__output_strong

function _is_installed() { dnf -q list installed $1; }

function _install() {
if [[ ! -v DEBUG || $DEBUG == 0 ]]; then
sudo dnf -qy install "$@"
elif [[ ! -v DEBUG || $DEBUG < -1 ]]; then
sudo &>/dev/null dnf -qy install "$@"
elif [[ ! -v DEBUG || $DEBUG < 0 ]]; then
sudo 1>/dev/null 2>&1 dnf -qy install "$@"
else
sudo dnf -y install "$@"
fi
}
export -f _install
__output_strong
;;

debian|ubuntu)
# '-qq' implies '-y'
# http://askubuntu.com/questions/258219/how-do-i-make-apt-get-install-less-noisy

#DISTRO_VERSION=$(lsb_release -r -s)
DISTRO_VERSION=$(lsb_release -d -s | cut -d' ' -f2)

__output_strong

function _is_installed() {
[[ "$(2>/dev/null /usr/bin/dpkg-query -W -f '${db:Status-Abbrev}' $1 | tr -d '[:space:]')" == 'ii' ]]
}

#Aquire::http::proxy Acquire::Retries::

function _install() { 
if (( DEBUG == 0 )); then
sudo 1>/dev/null DEBIAN_FRONTEND=noninteractive apt-get -q -y -o=Dpkg::Use-Pty=0 install "$@"
elif (( DEBUG < -1 )); then
sudo &>/dev/null DEBIAN_FRONTEND=noninteractive apt-get -qq -o=Dpkg::Use-Pty=0 install "$@"
elif (( DEBUG < -0 )); then
sudo 1>/dev/null 2>&1 DEBIAN_FRONTEND=noninteractive apt-get -qq -o=Dpkg::Use-Pty=0 install "$@"
else
sudo DEBIAN_FRONTEND=noninteractive apt-get -y -o=Dpkg::Use-Pty=0 install "$@"
fi
}
export -f _install
__output_strong

__output
export DISTRO_VERSION=$DISTRO_VERSION
export DISTRO_MAJOR_VERSION=$(echo $DISTRO_VERSION | /usr/bin/cut -d. -f1)
export DISTRO_MINOR_VERSION=$(echo $DISTRO_VERSION | /usr/bin/cut -d. -f2)
export DISTRO_REVISION=$(echo $DISTRO_VERSION | /usr/bin/cut -d. -f3)
__output

[[ -n "$SYSTEMD_VERSION" ]] && {
__output
export SYSTEMD_VERSION=$SYSTEMD_VERSION
__output
}

;;

esac


:<<\_c
version() - convert dotted vertion to whole number so it may be compared to other whole-number versions
_c
:<<\__s
__output_strong

function version() { echo "$@" | awk -F. '{ printf("%d%03d%03d%03d\n", $1,$2,$3,$4); }'; }
export -f version
__output_strong
__s

true

