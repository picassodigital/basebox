#!/usr/bin/env bash
[[ -v DEBUG ]] && (( DEBUG >= 0 )) && printf "\e[1;38;5;228m>>> %s @: %s\e[0m\n" "${BASH_SOURCE[0]}" "${*@Q}"

#echo "trigger.sh <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<1"
#DEBUG=2

#echo "trigger.sh @: $@"

:<<\_c
source first so subsequent configuration can override these original settings
_c

. $PR/bin/provider.env

. guest-environment.sh  # <- $PR/$PV/bin/xxx-*.*, $PR/bin/xxx-*.*, $PICASSO_HOME_DOTDIR/bin/xxx-*.* -> _debug()...

:<<\_c
$PR/$PV/bin/trigger.sh

see: $PROOF/picasso/trigger/
_c

:<<\_c
we load _debug functions just for support between here and the bottom of the file when we run '. guest-environment.sh'
this would not be necessary if there were no debugging lines between here and '. guest-environment.sh'
_c

#echo "trigger.sh <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<2"
#DEBUG=2

_debug "trigger PROVISIONER_USER: $PROVISIONER_USER, whoami: $(whoami)"


# ----------
:<<\_c
we need to force PROVISIONER_USER=root to avoid re-entry and allow this script to complete...
PROVISIONER_USER=root . $PR/bin/picasso-guest.sh

however; we have this built-in design...
prov-sys.sh -> $PR/bin/provider.env -> $PROVISIONER_USER

picasso-guest.sh {
trigger.sh -> $PICASSO_HOME_DOTDIR/bin/020-provisioner.env -> $PROVISIONER_USER
}

therefore, we need to source picasso-guest.sh before creating either of those files
_c

:<<\_c
NB: this file is incorporated into each basebox - iow: changes to this file require a rebuild of the basebox

this script receives arguments that Vagrant passes to it that originate within the Vagrantfile
in addition, Vagrant also sets the environment variable PROVISIONER=<name of current provisioning script file>
note that arguments are received as pairs (--<name>=[value])

[assumptions]
provisioner-env) may require vboxsf

[usage]
. guest-environment.sh #$@
...
_c


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
:<<\_c
only the first provisioner processes this...
chained provisioners never get here
_c

ARG0=$0

#echo slddslfslsdfsdf
#OPTARGS="$@" . longopts.sh  # <- $@(--*=*) -> longopts[], longvals[], $@
#. longopts.sh $@  # <- $@(--*=*) -> longopts[], longvals[], $@
. longopts.sh #$@  # <- $@(--*=*) -> longopts[], longvals[], $@

_debug3 "longopts: ${longopts[@]}"

#other=()

PROVISIONER_NAME=unknown
PROVISIONER_USER=picasso  # hard-coded default in case it's not declared
PICASSO_HOME_DOTDIR=/home/$PROVISIONER_USER/.picasso
PROVISIONER_ENV=$PICASSO_HOME_DOTDIR/bin/020-provisioner.env

for i in ${!longopts[@]}; do
opt=${longopts[$i]}
val=${longvals[$i]}

_debug3 "opt: $opt, val: $val"

case $opt in

provisioner-name) PROVISIONER_NAME=$val;;

provisioner-user) PROVISIONER_USER=$val;;

provisioner-env)

:<<\_c
provisioner.sh scripts have no knowledge that run-once trigger script preceded it
they simply have to follow convention of the trigger's results like the existence of $PICASSO_HOME_DOTDIR/bin/020-provisioner.env
_c

_info "Loading provisioner-env from $val"

  if [[ "$val" =~ (^| )http:// ]]; then

    curl -s -o $PROVISIONER_ENV $val

  elif [[ -f "$val" ]]; then

    cp "$val" $PROVISIONER_ENV

  else
    _error_exit "File not found: $val"
  fi

:<<\_s
cat >> $PROVISIONER_ENV <<-!
  METADATA_SOURCE=$val
!
_s

;;


esac
:<<\_j

*)
if [[ -n "$val" ]]; then
other+=("--${opt}=$val")
else
other+=("--${opt}")
fi
;;
_j

done

#set -- "${other[@]}" # restore other parameters

_debug "sdzzozswwls PROVISIONER_NAME: $PROVISIONER_NAME, PROVISIONER_USER: $PROVISIONER_USER, PROVISIONER_ENV: $PROVISIONER_ENV"

#echo "dsfsf OPTARGS: $OPTARGS"

[[ -f $PROVISIONER_ENV ]] && chown $PROVISIONER_USER:$PROVISIONER_USER $PROVISIONER_ENV


# ----------
_debug "PROVISIONER_USER: $PROVISIONER_USER, USER: $USER"

[[ "$PROVISIONER_USER" != "$USER" ]] && {

_debug "sudo -Hin -u $PROVISIONER_USER \"$ARG0\" $@"

#  sudo -Hin -u $PROVISIONER_USER "$ARG0" $OPTARGS; exit $?  # re-enter!
  sudo -Hin -u $PROVISIONER_USER "$ARG0" $@; exit $?  # re-enter!
}


# ----------
:<<\_c
this process is deferred until after the user account (if any) has been initialized, in case the account is the target of the file copy
_c

_debug_pcopy "PASS_FILE_SRC: $PASS_FILE_SRC, PASS_FILE_DEST: $PASS_FILE_DEST"

[[ -n "$PASS_FILE_SRC" ]] && {

#whoami
#ls -l ~/

PASS_FILE_SRC=($PASS_FILE_SRC)  # unserialize
PASS_FILE_DEST=($PASS_FILE_DEST)  # unserialize

_debug_pcopy "PASS_FILE_SRC[@]: ${PASS_FILE_SRC[@]}, PASS_FILE_DEST[@]: ${PASS_FILE_DEST[@]}"

for ((i=0; i<${#PASS_FILE_SRC[@]}; i++)); do

[[ -d "${PASS_FILE_DEST[$i]}" ]] || {

dir=$(dirname ${PASS_FILE_DEST[$i]})

_debug_pcopy "dir: $dir"

[[ -d $dir ]] || {

_debug_pcopy "mkdir -p $dir"

:<<\_s
sudo mkdir -p $dir || _error_exit "sudo mkdir -p $dir"
[[ -n "$PROVISIONER_USER" ]] && sudo chown -R $PROVISIONER_USER:$PROVISIONER_USER $dir
_s

mkdir -p $dir || _error_exit "mkdir -p $dir"

#echo sflwoowsdfs
#whoami
#ls -l $PR/
}
}

_debug_pcopy "$(ls -l $HOME)"

_debug_pcopy "cp ${PASS_FILE_SRC[$i]} ${PASS_FILE_DEST[$i]}"

:<<\_s
sudo cp ${PASS_FILE_SRC[$i]} ${PASS_FILE_DEST[$i]} || _error_exit "sudo cp ${PASS_FILE_SRC[$i]} ${PASS_FILE_DEST[$i]}"
[[ -n "$PROVISIONER_USER" ]] && sudo chown $PROVISIONER_USER:$PROVISIONER_USER ${PASS_FILE_DEST[$i]}
_s
cp ${PASS_FILE_SRC[$i]} ${PASS_FILE_DEST[$i]} || _error_exit "cp ${PASS_FILE_SRC[$i]} ${PASS_FILE_DEST[$i]}"

#echo sflwoow
#ls -l ${PASS_FILE_DEST[$i]}
done
}


# ----------
[[ -f "$PROVISIONER_ENV" ]] || {

# provisioner.env was not specified as an argument so assume cloudinit functionality

LOCAL_METADATA_URL=http://169.254.169.253:8080/$HOSTNAME  # hard-coded

_debug "assume cloudinit LOCAL_METADATA_URL: $LOCAL_METADATA_URL"

if curl -s -o $PROVISIONER_ENV $LOCAL_METADATA_URL/provisioner.env; then

cat >> $PROVISIONER_ENV <<!
METADATA_SOURCE=cloudinit
LOCAL_METADATA_URL=$LOCAL_METADATA_URL
!

else
_warn "Local metadata file not found: $$LOCAL_METADATA_URL/provisioner.env"
fi
}


# ----------
if [[ -f "$PROVISIONER_ENV" ]]; then

#chmod 644 $PROVISIONER_ENV
#chown $PROVISIONER_USER:$PROVISIONER_USER $PROVISIONER_ENV

_debug3 "$(<$PROVISIONER_ENV)"

. $PROVISIONER_ENV  # -> $PROVISIONER_ENV

else
_warn "Provisioner file not found."
fi


# ----------
_debug2 "PROVISIONER_ENV: $PROVISIONER_ENV, METADATA_SOURCE: $METADATA_SOURCE, LOCAL_METADATA_URL: $LOCAL_METADATA_URL"

:<<\_c
$PICASSO_HOME_DOTDIR/bin/020-provisioner.env is already loaded and it may declare $PROVISIONER_ENV

the origin of PROVISIONER_ENV may be cloudinit script or a virtualbox argument

NB: we append to any existing file
_c

:<<\_s
[[ -n "$PROVISIONER_ENV" ]] && {

case "$METADATA_SOURCE" in

metafile)
_info "Loading metadata from /vagrant/$PROVISIONER_ENV"
cat /vagrant/$PROVISIONER_ENV >> $PROVISIONER_ENV
;;

cloudinit)
_info "Loading metadata from $LOCAL_METADATA_URL/$PROVISIONER_ENV"
curl -s $LOCAL_METADATA_URL/$PROVISIONER_ENV >> $PROVISIONER_ENV
;;

*) _error "Unknown METADATA_SOURCE: $METADATA_SOURCE";;

esac

_debug2 "$(ls -l $PROVISIONER_ENV)"
_debug3 "$(cat $PROVISIONER_ENV)"
}
_s

#echo shgsfslfsdlfs
:<<\_s
#defer loading
[[ -f "$PROVISIONER_ENV" ]] && {

echo swwowroworwowrowrwr
. $PROVISIONER_ENV
}
_s

_debug "trigger4 PROVISIONER_USER: $PROVISIONER_USER"

# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
_debug3 "_run_once_on_entry"

_debug "READONLY_MNT: $READONLY_MNT"
_debug "PICASSO_GIT_ENDPOINT: $PICASSO_GIT_ENDPOINT"

# if READONLY_MNT is defined, prov-cifs.sh will mount it

if [[ -n "$READONLY_MNT" ]]; then

_debug2 "xcvbsdwwewyisddsz READONLY_MNT: $READONLY_MNT"
_debug3 "$(ls -l $READONLY_MNT)"
_debug3 "$(ls -l $PR/v)"

:<<\_c
if the directory already exists, then it has already been initialized
it may already have been initialized within the basebox
or, it may already have been initialized by earlier chained provisioning

the question of whether or not to overwrite the existing directory arises
if we don't overwrite, then a new basebox must be used that contains the desired files?
if we do overwrite, then how do we do so only once and not by each chained provisioner?

philosophy...
that done within the basebox remains, and therefore, during basebox building, we don't initialize these directories
iow: $PR/core is pre-initialized within the basebox and does not get updated - use the basebox with the 'guest' files you need, because at provision time you cammot modify 'guest' files
other modules are installed on a first come, first served, basis
first come first served makes sense during provisioning, since the files should not change mid-provisioning
_c
:<<\_c
note that we are installing into $PR/$PV and not $PR/v. They may be different.
_c

[[ -d "$PR/$PV/instance" ]] || {
mkdir -p $PR/$PV/instance
cp -fr $READONLY_MNT/$PV/instance/* $PR/$PV/instance/
}

elif [[ -n "$PICASSO_GIT_ENDPOINT" ]]; then

_debug2 "xcvbsdwewwyisddsz PICASSO_GIT_ENDPOINT: $PICASSO_GIT_ENDPOINT"

:<<\_c
basebox.git is statically cloned into each prebuilt basebox

this has already been done in $DESKSP/picasso/basebox/*/*/pre-provisioner.sh...

[[ -d "$PR/$PV/basebox" ]] || { mkdir -p $PR/$PV/basebox; curl -s $PICASSO_GIT_ENDPOINT/tar/raw/master/basebox.tar.gz | tar -xz -C $PR/$PV/; }
_c

:<<\_c
instance.git is dynamically cloned into each provisioned instance
_c

[[ -d "$PR/$PV/instance" ]] || {

git clone $PICASSO_GIT_ENDPOINT/instance.git $PR/$PV/instance
#git clone $PICASSO_GIT_ENDPOINT/$PV/instance.git $PR/$PV/instance

cp $PR/$PV/instance/bin/* $PR/$PV/bin/  # overlay bin/* to save having to add each repo's bin path to $PATH
}

:<<\_x
[[ -f "$PR/$PV/instance.txt" ]] || {
curl -s $PICASSO_GIT_ENDPOINT/tar/raw/master/instance.tar.gz | tar -xz -C $PR/$PV/
}
_x
else
_error "Unknown PICASSO_GIT_ENDPOINT: $PICASSO_GIT_ENDPOINT"
fi

_debug3 "$(ls -la $PR/v)"


# this is deferred to last...

:<<\_c
picasso-guest.sh is designed to re-enter its calling script if [[ $PROVISIONER_USER != $USER ]]
we do not re-enter this script due to the symlink change above 
the re-entry overlays the current shell with a new one and, most importantly, to terminate execution within the current shell
iow: if [[ $PROVISIONER_USER != $USER ]] this script will not execute anything below the sourcing of picasso-guest.sh
the provisioner is the target to the re-entery
_c

:<<\_c
only run this script file once
$PR/bin/picasso-guest.sh linked to $PR/$PV/bin/real-picasso-guest.sh indicates the script has run
_c
ln -sf $PR/$PV/bin/real-picasso-guest.sh $PR/bin/picasso-guest.sh

chown $PROVISIONER_USER:$PROVISIONER_USER $PR/bin/picasso-guest.sh

_debug "trigger5 @: $@"

#. $PR/bin/picasso-guest.sh "$OPTARGS"  # the new real-picasso-guest.sh
#. $PR/bin/picasso-guest.sh $OPTARGS  # the new real-picasso-guest.sh
. $PR/bin/picasso-guest.sh $@  # the new real-picasso-guest.sh

_debug "trigger done"
